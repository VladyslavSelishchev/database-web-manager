﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using DbWebManager.DAL;
using DbWebManager.Models;


namespace DbWebManager.BLL
{
    public class WorkspaceDbConnector : IWorkspaceDbConnector
    {
        public IMsSqlConnector SqlConnector { get; private set; }


        public WorkspaceDbConnector(MsSqlConnectionModel connectionModel)
        {
            // добавляем свойства, которые будут заполняться по умолчанию - добы быстрее работал foreach
            List<string> defaultInitFields = new List<string>();

            defaultInitFields.Add("IsSystemObject");
            defaultInitFields.Add("Name");
            defaultInitFields.Add("ID");
            defaultInitFields.Add("CreateDate");

            List<string> defaultInitFieldsForVPF = new List<string>();
            defaultInitFieldsForVPF.AddRange(defaultInitFields);
            defaultInitFieldsForVPF.Add("Schema");
            defaultInitFieldsForVPF.Add("DateLastModified");

            IMsSqlConnector connector = new MsSqlConnector(connectionModel,
                defaultInitFields,
                defaultInitFieldsForVPF,
                defaultInitFieldsForVPF,
                defaultInitFieldsForVPF,
                defaultInitFieldsForVPF);
            this.SqlConnector = connector;
        }


        public void TryConnect()
        {
            SqlConnector.TryConnectToSqlServer();
        }


        public DataTable ReadTableData(int databaseId, int tableId)
        {
            var database = SqlConnector.GetDatabase(databaseId);
            var table = database.Tables.ItemById(tableId);

            StringBuilder queryString = new StringBuilder("SELECT * FROM ");
            queryString.Append(table.Name);

            DataSet resultDataSet = database.ExecuteWithResults(queryString.ToString());

            DataTable resultDataTable = resultDataSet.Tables[0];

            StringBuilder name = new StringBuilder(table.Parent.ToString());
            name.Append('.');
            name.Append(table.ToString());

            resultDataTable.TableName = name.ToString();

            return resultDataTable;
        }

        public DataTable ReadViewData(int databaseId, int viewId)
        {
            var database = SqlConnector.GetDatabase(databaseId);
            var view = database.Views.ItemById(viewId);

            StringBuilder queryString = new StringBuilder("SELECT * FROM ");
            queryString.Append(view.ToString());

            DataSet resultDataSet = database.ExecuteWithResults(queryString.ToString());

            DataTable resultDataTable = resultDataSet.Tables[0];

            StringBuilder name = new StringBuilder(view.Parent.ToString());
            name.Append('.');
            name.Append(view.ToString());

            resultDataTable.TableName = name.ToString();

            return resultDataTable;
        }


        public void UpdateViewStructure(int databaseId, int viewId, string viewBody)
        {
            var view = SqlConnector.GetView(databaseId, viewId);
            view.TextBody = viewBody;
            view.Alter();
        }

        public void UpdateProcedureStructure(int databaseId, int procedureId, string procedureDody)
        {
            var storedProcedure = SqlConnector.GetStoredProcedure(databaseId, procedureId);
            storedProcedure.TextBody = procedureDody;
            storedProcedure.Alter();
        }

        public void UpdateUserDefinedFunctionStructure(int databaseId, int functionId, string functionDody)
        {
            var userDefinedFunction = SqlConnector.GetUserDefinedFunction(databaseId, functionId);
            userDefinedFunction.TextBody = functionDody;
        }


        public bool DeleteData(string node, string nodeType)
        {
            bool result = true;
            string[] splitNode = node.Split('&');

            if (splitNode.Length == 1)
            {
                int databaseID = 0;
                string[] database = splitNode[0].Split('=');
                if (database[0].ToLower() == "database")
                {
                    databaseID = Convert.ToInt32(database[1]);
                }

                SqlConnector.DeleteDatabase(databaseID);
            }
            else
            {
                try
                {
                    string[] databasePart = splitNode[0].Split('=');
                    string[] element = splitNode[1].Split('=');
                    int databaseID = Convert.ToInt32(databasePart[1]);
                    int elementID = Convert.ToInt32(element[1]);
                    switch (nodeType)
                    {
                        case "Table":
                            SqlConnector.DeleteTable(databaseID, elementID);
                            break;
                        case "StoredProcedure":
                            SqlConnector.DeleteStoredProcedure(databaseID, elementID);
                            break;
                        case "View":
                            SqlConnector.DeleteView(databaseID, elementID);
                            break;
                        case "UserDefinedFunction":
                            SqlConnector.DeleteUserDefinedFunction(databaseID, elementID);
                            break;
                        default:
                            break;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }

            return result;
        }

        public DataSet ExecuteQuery(int databaseId, string sqlQuery)
        {
            return SqlConnector.ExecuteQuery(databaseId, sqlQuery);
        }

        public DataSet ExecuteQuery(string sqlQuery)
        {
            return SqlConnector.ExecuteQuery(sqlQuery);
        }


        public void UpdateDataTable(int databaseId, int tableId, DataTable dataTable)
        {
            var database = SqlConnector.GetDatabase(databaseId);
            var table = SqlConnector.GetTable(databaseId, tableId);

            SqlConnection sqlConn = null;
            SqlDataAdapter myDataAdapter = null;
            SqlCommandBuilder cmdBuilder = null;
            try
            {
                using (sqlConn = new SqlConnection(SqlConnector.MsSqlServer.ConnectionContext.ConnectionString))
                {
                    using (
                        myDataAdapter =
                            new SqlDataAdapter("SELECT * FROM " + database.Name + ".dbo." + table.Name, sqlConn))
                    {
                        sqlConn.Open();

                        DataSet dataSetFromServer = new DataSet();

                        myDataAdapter.Fill(dataSetFromServer);

                        // генерируем команды для операций update, insert и delete 
                        using (cmdBuilder = new SqlCommandBuilder(myDataAdapter))
                        {
                            myDataAdapter.InsertCommand = cmdBuilder.GetInsertCommand();
                            myDataAdapter.UpdateCommand = cmdBuilder.GetUpdateCommand();
                            myDataAdapter.DeleteCommand = cmdBuilder.GetDeleteCommand();

                            DataTable tableFromServer = dataSetFromServer.Tables[0];

                            tableFromServer = dataTable;

                            myDataAdapter.Update(tableFromServer);
                            tableFromServer.AcceptChanges();
                            dataTable.AcceptChanges();
                            myDataAdapter.Fill(tableFromServer);
                            myDataAdapter.Fill(dataTable);
                        }

                        sqlConn.Close();
                    }
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            finally
            {
                if (cmdBuilder != null)
                {
                    cmdBuilder.Dispose();
                }
                if (myDataAdapter != null)
                {
                    myDataAdapter.Dispose();
                }
                if (sqlConn != null)
                {
                    sqlConn.Dispose();
                }
            }
        }
    }
}