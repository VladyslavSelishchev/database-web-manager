﻿using Ext.Net.MVC;

namespace DbWebManager.UI
{
    public interface ITreeService
    {
        /// <summary>
        /// Get Nodes in StoreResult by parent node ID
        /// </summary>
        /// <param name="node">Parent node ID</param>
        /// <returns>Nodes collection</returns>
        StoreResult GetNode(string node);
    }
}