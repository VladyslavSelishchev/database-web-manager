﻿using System.Collections.Generic;
using System.Data;
using DbWebManager.DAL;
using DbWebManager.Models;

namespace DbWebManager.BLL
{
    /// <summary>
    /// Connects to the server TreeService
    /// </summary>
    public class TreeDbConnector : ITreeDbConnector
    {
        private IMsSqlConnector msSqlConnector;


        public TreeDbConnector(MsSqlConnectionModel connectionModel)
        {
            // добавляем свойства, которые будут заполняться по умолчанию - добы быстрее работал foreach
            List<string> defaultInitFields = new List<string>();
            defaultInitFields.Add("IsSystemObject");
            defaultInitFields.Add("Name");
            defaultInitFields.Add("ID");

            IMsSqlConnector sqlConnector = new MsSqlConnector(connectionModel,
                defaultInitFields,
                defaultInitFields,
                defaultInitFields,
                defaultInitFields,
                defaultInitFields);

            this.msSqlConnector = sqlConnector;
        }


        private Dictionary<string, string> GetObjectDictionary<T>(IEnumerable<T> objectEnumerable)
        {
            Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

            foreach (dynamic obj in objectEnumerable)
            {
                resultDictionary.Add(obj.ID.ToString(), obj.Name);
            }
            return resultDictionary;
        }

        // -------------------------------------------------------------------------------------------------------

        public Dictionary<string, string> GetSystemDatabasesDictionary()
        {
            Dictionary<string, string> resultDictionary = new Dictionary<string, string>();
            foreach (var database in msSqlConnector.GetSystemDatabases())
            {
                resultDictionary.Add(database.ID.ToString(), database.Name);
            }
            return resultDictionary;
        }

        public Dictionary<string, string> GetUserDatabasesDictionary()
        {
            Dictionary<string, string> resultDictionary = new Dictionary<string, string>();
            foreach (var database in msSqlConnector.GetUserDatabases())
            {
                resultDictionary.Add(database.ID.ToString(), database.Name);
            }
            return resultDictionary;
        }

        public Dictionary<string, string> GetUserTablesDictionary(int databaseId)
        {
            return GetObjectDictionary(msSqlConnector.GetUserTables(databaseId));
        }

        public Dictionary<string, string> GetSystemTablesDictionary(int databaseId)
        {
            return GetObjectDictionary(msSqlConnector.GetSystemTables(databaseId));
        }

        public Dictionary<string, string> GetUserViewsDictionary(int databaseId)
        {
            return
                GetObjectDictionary(msSqlConnector.GetUserViews(databaseId));
        }

        public Dictionary<string, string> GetSystemViewsDictionary(int databaseId)
        {
            return
                GetObjectDictionary(msSqlConnector.GetSystemViews(databaseId));
        }


        public Dictionary<string, string> GetUserStoredProceduresDictionary(int databaseId)
        {
            return
                GetObjectDictionary(msSqlConnector.GetUserStoredProcedures(databaseId));
        }

        public Dictionary<string, string> GetSystemStoredProceduresDictionary(int databaseId)
        {
            return
                GetObjectDictionary(msSqlConnector.GetSystemStoredProcedures(databaseId));
        }


        public Dictionary<string, string> GetUserFunctionsDictionary(int databaseId)
        {
            return GetObjectDictionary(msSqlConnector.GetUserFunctions(databaseId));
        }


        public Dictionary<string, string> GetSystemFunctionsDictionary(int databaseId)
        {
            return GetObjectDictionary(msSqlConnector.GetSystemFunctions(databaseId));
        }


        public Dictionary<string, string> GetSystemTableValuedFunctionsDictionary(int databaseId)
        {
            Dictionary<string, string> resultDictionary = new Dictionary<string, string>();
            DataSet resultDataSet = msSqlConnector.GetDatabase(databaseId).ExecuteWithResults(
                "SELECT object_id, name FROM sys.all_objects WHERE type_desc LIKE '%TABLE_VALUED_FUNCTION%' AND is_ms_shipped = 1");

            foreach (DataRow row in resultDataSet.Tables[0].Rows)
            {
                resultDictionary.Add(row["object_id"].ToString(), row["name"].ToString());
            }
            return resultDictionary;
        }

        public Dictionary<string, string> GetSystemScalarValuedFunctionsDictionary(int databaseId)
        {
            Dictionary<string, string> resultDictionary = new Dictionary<string, string>();
            DataSet resultDataSet = msSqlConnector.GetDatabase(databaseId).ExecuteWithResults(
                "SELECT object_id, name FROM sys.all_objects WHERE type_desc LIKE '%SQL_SCALAR_FUNCTION%' AND is_ms_shipped = 1");

            foreach (DataRow row in resultDataSet.Tables[0].Rows)
            {
                resultDictionary.Add(row["object_id"].ToString(), row["name"].ToString());
            }
            return resultDictionary;
        }

        // --------------------------------------------------------------------------------------------------------------------


        public Dictionary<string, string> GetTableColumnsDictionary(int databaseId, int tableId)
        {
            return GetObjectDictionary(msSqlConnector.GetTableColumns(databaseId, tableId));
        }

        public Dictionary<string, string> GetViewColumnsDictionary(int databaseId, int viewId)
        {
            return GetObjectDictionary(msSqlConnector.GetViewsColumns(databaseId, viewId));
        }

        public Dictionary<string, string> GetTableForeignKeysDictionary(int databaseId, int tableId)
        {
            return GetObjectDictionary(msSqlConnector.GetTableForeignKeys(databaseId, tableId));
        }

        public Dictionary<string, string> GetTableTriggersDictionary(int databaseId, int tableId)
        {
            return GetObjectDictionary(msSqlConnector.GetTableTriggers(databaseId, tableId));
        }

        public Dictionary<string, string> GetViewTriggersDictionary(int databaseId, int viewId)
        {
            return GetObjectDictionary(msSqlConnector.GetViewTriggers(databaseId, viewId));
        }

        public Dictionary<string, string> GetTableIndexesDictionary(int databaseId, int tableId)
        {
            return GetObjectDictionary(msSqlConnector.GetTableIndexes(databaseId, tableId));
        }

        public Dictionary<string, string> GetViewIndexesDictionary(int databaseId, int viewId)
        {
            return GetObjectDictionary(msSqlConnector.GetViewIndexes(databaseId, viewId));
        }
    }
}