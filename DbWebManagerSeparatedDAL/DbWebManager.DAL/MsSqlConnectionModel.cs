﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbWebManager.DAL
{
    public class MsSqlConnectionModel
    {
        [Required(ErrorMessage = "Please specify the server name")]
        [Display(Name = "Server name")]
        public string ServerName { get; set; }

        [Required(ErrorMessage = "Please select authentication method")]
        [Display(Name = "Use Windows authentication")]
        public bool UseWindowsIntegratedSecurity { get; set; }

        [Display(Name = "SQL login")]
        public string UserName { get; set; }

        [DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
}
