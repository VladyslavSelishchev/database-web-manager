﻿using System.Data;
using DbWebManager.Models;
using Ext.Net;

namespace DbWebManager.UI
{
    public interface IWorkspaceService
    {
        void TryConnect();

        /// <summary>
        /// Get new Panel depending on the clicked TreePanel node Id
        /// </summary>
        /// <param name="node"></param>
        /// <param name="nodeType"></param>
        /// <returns></returns>
        Panel GetPanel(string node, string nodeType);

        /// <summary>
        /// Save modified data from Table to Server
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="dataSetFromServer"></param>
        /// <param name="changedTable"></param>
        void SaveDataTableToServer(string parameters, DataSet dataSetFromServer, ChangedDataTable changedTable);

        Ext.Net.MVC.PartialViewResult GetDataFromTable(string parameters);

        bool DeleteData(string node, string nodeType);

        Panel GetPanelFromQuery(string queryTxt, int index);

        Panel GetPanelToQuery(string defaultQueryText, int index);
    }
}