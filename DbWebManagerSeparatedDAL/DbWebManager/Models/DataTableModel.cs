﻿using System.Collections.Generic;
using System.Data;
using Ext.Net;

namespace DbWebManager.Models
{
    public class DataTableModel
    {
        public string Parameters { get; set; }

        public DataTable CurrentTable { get; set; }

        public List<Button> Buttons { get; set; }

        public List<ModelField> Fields { get; set; }

        public List<ColumnBase> Columns { get; set; }
    }
}