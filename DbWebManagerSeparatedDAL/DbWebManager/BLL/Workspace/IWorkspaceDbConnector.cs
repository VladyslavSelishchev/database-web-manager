﻿using System.Data;
using Microsoft.SqlServer.Management.Smo;
using DbWebManager.DAL;

namespace DbWebManager.BLL
{
    public interface IWorkspaceDbConnector
    {
        IMsSqlConnector SqlConnector { get; }


        void TryConnect();

        /// <summary>
        /// Read data from Table in DataTable
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="tableId"></param>
        /// <returns></returns>
        DataTable ReadTableData(int databaseId, int tableId);

        /// <summary>
        /// Read data from View in DataTable
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="viewId"></param>
        /// <returns></returns>
        DataTable ReadViewData(int databaseId, int viewId);

        // ---------------------------------------------------------

        /// <summary>
        /// Update changed data in DataTable from Table on server
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="tableId"></param>
        /// <param name="dataTable"></param>
        void UpdateDataTable(int databaseId, int tableId, DataTable dataTable);


        // ---------------------------------------------------------


        void UpdateViewStructure(int databaseId, int viewId, string viewDody);

        void UpdateProcedureStructure(int databaseId, int procedureId, string procedureDody);

        void UpdateUserDefinedFunctionStructure(int databaseId, int functionId, string functionDody);

        // ---------------------------------------------------------

        // Database CreateNewDatabase(string newDatabaseName);

        bool DeleteData(string node, string nodeType);


        DataSet ExecuteQuery(int databaseId, string sqlQuery);

        DataSet ExecuteQuery(string sqlQuery);
    }
}