﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using System.Data;
using DbWebManager.Models;
using DbWebManager.DAL;
using DbWebManager.UI;

namespace DbWebManager.Controllers
{
    public class ManagerController : Controller
    {
        /// <summary>
        /// Try connect or check connection to current server with settings from Session
        /// </summary>
        /// <returns>MsSqlManager or null</returns>
        private IWorkspaceService GetConnectionFromSession()
        {
            MsSqlConnectionModel connectionModel = Session["ConnectionModel"] as MsSqlConnectionModel;

            try
            {
                IWorkspaceService workspaceService = new WorkspaceService(connectionModel);
                workspaceService.TryConnect();
                return workspaceService;
            }
            catch (Exception)
            {
                // если модель неверна
                Session.Clear();
                return null;
            }
        }

        /// <summary>
        /// Show basic user interface
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            IWorkspaceService workspaceService = GetConnectionFromSession();
            if (workspaceService == null)
            {
                return RedirectToAction("Index", "Login");
            }

            TabPanel mainTabPanel = X.GetCmp<TabPanel>("MainTabPanel");

            if (Session["ExceptionObject"] == null)
            {
                // ServerInfoPanel
                Panel serverInfoPanel = workspaceService.GetPanel("Folder=serverInfoPanel", "Folder");
                if (serverInfoPanel != null)
                {
                    serverInfoPanel.AddTo(mainTabPanel);
                    mainTabPanel.SetActiveTab(serverInfoPanel.ID);
                }
            }
            else
            {
                Exception exception = (Exception) Session["ExceptionObject"];

                Panel errorPanel = new Panel();
                errorPanel.ID = "ErrorInfoPanel";
                errorPanel.Title = "Error";
                errorPanel.Padding = 5;
                errorPanel.Closable = true;
                errorPanel.Html = exception.Message;

                errorPanel.AddTo(mainTabPanel);
                mainTabPanel.SetActiveTab(errorPanel.ID);

                Session["ExceptionObject"] = null;
            }

            return View();
        }

        /// <summary>
        /// Try delete object from server
        /// </summary>
        /// <returns></returns>
        public ActionResult DeleteData()
        {
            IWorkspaceService workspaceService = GetConnectionFromSession();
            if (workspaceService == null)
            {
                return RedirectToAction("Index", "Login"); // View(model);
            }

            string node = Convert.ToString(Session["node"]);
            string nodeType = Convert.ToString(Session["nodeType"]);

            bool result = workspaceService.DeleteData(node, nodeType);

            if (result)
            {
                X.Msg.Notify(new NotificationConfig()
                {
                    Title = "Deleted",
                    Html = "<b>" + nodeType + " has been deleted!",
                    Width = 250,
                    Height = 150
                }).Show();
            }

            return this.Direct();
        }


        public ActionResult ShowData()
        {
            IWorkspaceService workspaceService = GetConnectionFromSession();
            if (workspaceService == null)
            {
                return RedirectToAction("Index", "Login"); // View(model);
            }

            string node = Convert.ToString(Session["node"]);
            string nodeType = Convert.ToString(Session["nodeType"]);

            TabPanel mainTabPanel = X.GetCmp<TabPanel>("MainTabPanel");

            Panel newPanel = workspaceService.GetPanel(node, nodeType);


            if (newPanel != null)
            {
                newPanel.AddTo(mainTabPanel);
                mainTabPanel.SetActiveTab(newPanel.ID);
            }


            return this.Direct();
        }

        /// <summary>
        /// Add new Tab in MainTabPanel depends of Tree node Id
        /// </summary>
        /// <param name="node"></param>
        /// <param name="nodeType"></param>
        /// <returns></returns>
        public ActionResult AddTab(string node = null, string nodeType = null)
        {
            IWorkspaceService workspaceService = GetConnectionFromSession();
            if (workspaceService == null)
            {
                return RedirectToAction("Index", "Login"); // View(model);
            }

            if (node == null || nodeType == null)
            {
                node = Convert.ToString(Session["node"]);
                nodeType = Convert.ToString(Session["nodeType"]);
            }

            TabPanel mainTabPanel = X.GetCmp<TabPanel>("MainTabPanel");

            Panel newPanel = workspaceService.GetPanel(node, nodeType);

            if (newPanel != null)
            {
                newPanel.AddTo(mainTabPanel);
                mainTabPanel.SetActiveTab(newPanel.ID);
            }


            return this.Direct();
        }


        /// <summary>
        /// Get Node Collection of next Node
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public StoreResult GetNode(string node)
        {
            IWorkspaceService workspaceService = GetConnectionFromSession();
            if (workspaceService == null)
            {
                return null;
            }

            MsSqlConnectionModel connectionModel = Session["ConnectionModel"] as MsSqlConnectionModel;
            ITreeService dbTree = new TreeService(connectionModel);
            return dbTree.GetNode(node);
        }


        // --------------------------------------------------------------------------------------------------

        /// <summary>
        /// Get data from Table and add to Panel for editing
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public ActionResult GetDataFromTable(string parameters)
        {
            IWorkspaceService workspaceService = GetConnectionFromSession();
            if (workspaceService == null)
            {
                return null;
            }

            Ext.Net.MVC.PartialViewResult result = workspaceService.GetDataFromTable(parameters);

            this.GetCmp<TabPanel>("MainTabPanel").SetLastTabAsActive();

            return result;
        }

        /// <summary>
        /// Save changes in DataTable to Server
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="handler"></param>
        /// <returns></returns>
        public ActionResult HandleChanges(string parameters, StoreDataHandler handler)
        {
            IWorkspaceService workspaceService = GetConnectionFromSession();
            if (workspaceService == null)
            {
                return RedirectToAction("Index", "Login"); // View(model);
            }

            ChangedDataTable newValues = Ext.Net.JSON.Deserialize<ChangedDataTable>(handler.JsonData);

            DataSet currentDataSet = ((DataSet) Session[parameters]).Copy();

            try
            {
                workspaceService.SaveDataTableToServer(parameters, currentDataSet, newValues);

                X.Msg.Notify(new NotificationConfig()
                {
                    Icon = Icon.Accept,
                    Title = "Changed",
                    Html = "<b>Values has been changed!</b>",
                    Width = 250,
                    Height = 150,
                    HideDelay = 5000,
                    Closable = true
                }).Show();

                TabPanel mainTabPanel = X.GetCmp<TabPanel>("MainTabPanel");
                Panel newPanel = workspaceService.GetPanel(parameters, "Table");

                if (newPanel != null)
                {
                    newPanel.AddTo(mainTabPanel);
                    mainTabPanel.SetActiveTab(newPanel.ID);
                }
            }
            catch (Exception Ex)
            {
                X.Msg.Notify(new NotificationConfig()
                {
                    Icon = Icon.Error,
                    Title = "Error!",
                    Html = "<b>Error while saving! Please check your data!</b><br/>" + Ex.Message,
                    Width = 300,
                    Height = 300,
                    HideDelay = 10000,
                    Closable = true
                }).Show();
            }

            return this.Direct();
        }


        // --------------------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------------------

        public ActionResult RunQuery(string queryTxt = null, string currentIndex = null)
        {
            IWorkspaceService workspaceService = GetConnectionFromSession();
            if (workspaceService == null)
            {
                return RedirectToAction("Index", "Login");
            }

            int index;
            try
            {
                index = Convert.ToInt32(currentIndex);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }

            TextArea queryTextArea = X.GetCmp<TextArea>("QueryTxt" + index);

            if (queryTxt == null)
            {
                queryTxt = queryTextArea.Text;
            }
            Panel newPanel = workspaceService.GetPanelFromQuery(queryTxt, index);

            if (newPanel != null)
            {
                //Добавляем форму в панель вкладок
                newPanel.AddTo(this.GetCmp<TabPanel>("MainTabPanel"));
                this.GetCmp<TabPanel>("MainTabPanel").SetActiveTab("queryResult" + index);

                X.Msg.Notify(new NotificationConfig()
                {
                    Title = "Query result",
                    Html = "<b>Query was executed correctly!",
                    Width = 250,
                    Height = 150
                }).Show();
            }
            else
            {
                X.Msg.Notify(new NotificationConfig()
                {
                    Title = "Error!",
                    Html = "<b>SQL Server: Query error!",
                    Width = 250,
                    Height = 100
                }).Show();
            }
            return this.Direct();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="defaultQueryText">Используется при создании новых View, Function, Procedure как стартовый шаблон</param>
        /// <returns></returns>
        public ActionResult AddQuery(string defaultQueryText = "")
        {
            IWorkspaceService workspaceService = GetConnectionFromSession();
            if (workspaceService == null)
            {
                return RedirectToAction("Index", "Login");
            }

            int index;
            //добавляем вкладку запроса
            if (Session["queryIndex"] == null)
            {
                Session["queryIndex"] = 1;
                index = Convert.ToInt32(Session["queryIndex"]);
            }
            else
            {
                index = Convert.ToInt32(Session["queryIndex"]);
                Session["queryIndex"] = ++index;
            }

            Panel newPanel = workspaceService.GetPanelToQuery(defaultQueryText, index);


            newPanel.AddTo(this.GetCmp<TabPanel>("MainTabPanel"));
            this.GetCmp<TabPanel>("MainTabPanel").SetActiveTab("query" + index);

            return this.Direct();
        }

        //Дисконектимся и выходим на окно логина
        public ActionResult disconnBtn_Click()
        {
            Session.Clear();
            return RedirectToAction("Index", "Login");
        }


        public ActionResult SelectItem(string node, string nodeType)
        {
            IWorkspaceService workspaceService = GetConnectionFromSession();
            if (workspaceService == null)
            {
                return RedirectToAction("Index", "Login");
            }

            Session["node"] = node;
            Session["nodeType"] = nodeType;

            return this.Direct();
        }
    }
}