﻿using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace DbWebManager.DAL
{
    

    public class MsSqlConnector : IMsSqlConnector
    {
        public Server MsSqlServer { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="serverAddress"></param>
        /// <param name="useWindowsIntegratedSecurity"></param>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <param name="connectionTimeout"></param>
        /// <param name="networkPacketSize"></param>
        /// <param name="encryptConnection"></param>
        /// <param name="protocol"></param>
        public MsSqlConnector(string serverAddress,
            bool useWindowsIntegratedSecurity,
            string login = null, string password = null,
            int? connectionTimeout = null,
            int? networkPacketSize = null,
            bool? encryptConnection = null,
            NetworkProtocol? protocol = null
            )
        {
            // https://msdn.microsoft.com/en-us/library/ms162132(v=sql.110).aspx
            //Connect to a remote instance of SQL Server. 
            MsSqlServer = new Server(serverAddress);

            if (useWindowsIntegratedSecurity == false)
            {
                MsSqlServer.ConnectionContext.LoginSecure = false; // set to true for Windows Authentication
                MsSqlServer.ConnectionContext.Login = login;
                MsSqlServer.ConnectionContext.Password = password;
            }

            if (connectionTimeout != null)
            {
                MsSqlServer.ConnectionContext.ConnectTimeout = (int)connectionTimeout;
            }
            if (networkPacketSize != null)
            {
                MsSqlServer.ConnectionContext.PacketSize = (int)networkPacketSize;
            }
            if (encryptConnection != null)
            {
                MsSqlServer.ConnectionContext.EncryptConnection = (bool)encryptConnection;
            }
            if (protocol != null)
            {
                MsSqlServer.ConnectionContext.NetworkProtocol = (NetworkProtocol)protocol;
            }
        }


        public MsSqlConnector(MsSqlConnectionModel connectionModel)
            : this(
                connectionModel.ServerName, connectionModel.UseWindowsIntegratedSecurity, connectionModel.UserName,
                connectionModel.Password)
        {
        }

        public MsSqlConnector(MsSqlConnectionModel connectionModel, List<string> defaultInitFieldsForDatabase,
            List<string> defaultInitFieldsForTable, List<string> defaultInitFieldsForView,
            List<string> defaultInitFieldsForStoredProcedure,
            List<string> defaultInitFieldsForUserDefinedFunction)
            : this(
                connectionModel.ServerName, connectionModel.UseWindowsIntegratedSecurity, connectionModel.UserName,
                connectionModel.Password)
        {
            AddServerSettings(defaultInitFieldsForDatabase,
                defaultInitFieldsForTable,
                defaultInitFieldsForView,
                defaultInitFieldsForStoredProcedure,
                defaultInitFieldsForUserDefinedFunction);
        }

        private void AddServerSettings(List<string> defaultInitFieldsForDatabase,
            List<string> defaultInitFieldsForTable, List<string> defaultInitFieldsForView,
            List<string> defaultInitFieldsForStoredProcedure,
            List<string> defaultInitFieldsForUserDefinedFunction)
        {
            // добавляем свойства, которые будут заполняться по умолчанию - добы быстрее работал foreach

            MsSqlServer.SetDefaultInitFields(typeof(Database), defaultInitFieldsForDatabase.ToArray());
            MsSqlServer.SetDefaultInitFields(typeof(Table), defaultInitFieldsForTable.ToArray());
            MsSqlServer.SetDefaultInitFields(typeof(View), defaultInitFieldsForView.ToArray());
            MsSqlServer.SetDefaultInitFields(typeof(StoredProcedure), defaultInitFieldsForStoredProcedure.ToArray());
            MsSqlServer.SetDefaultInitFields(typeof(UserDefinedFunction),
                defaultInitFieldsForUserDefinedFunction.ToArray());
        }


        public void TryConnectToSqlServer()
        {
            int buildNumber = MsSqlServer.BuildNumber;
        }

        /// <summary>
        /// Get enumerable collection of system Databases
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Database> GetSystemDatabases()
        {
            IEnumerable<Database> rezult = (from Database database in MsSqlServer.Databases
                                            where database.IsSystemObject || database.IsDatabaseSnapshot
                                            select database
                );

            return rezult;
        }

        // ------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Get enumerable collection of user Databases
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Database> GetUserDatabases()
        {
            IEnumerable<Database> rezult = (from Database database in MsSqlServer.Databases
                                            where !database.IsSystemObject && !database.IsDatabaseSnapshot
                                            select database
                );

            return rezult;
        }

        // ===========================================================================================================

        // ------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Get enumerable collection of Tables in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        public IEnumerable<Table> GetTables(int databaseId)
        {
            return GetTablesCollection(databaseId).Cast<Table>();
        }

        /// <summary>
        /// Get enumerable collection of user Tables in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        public IEnumerable<Table> GetUserTables(int databaseId)
        {
            IEnumerable<Table> rezult = (from Table table in GetDatabase(databaseId).Tables
                                         where !table.IsSystemObject
                                         select table
                );

            return rezult;
        }

        /// <summary>
        /// Get enumerable collection of system Tables in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        public IEnumerable<Table> GetSystemTables(int databaseId)
        {
            IEnumerable<Table> rezult = (from Table table in GetDatabase(databaseId).Tables
                                         where table.IsSystemObject
                                         select table
                );

            return rezult;
        }


        private TableCollection GetTablesCollection(int databaseId)
        {
            Database database = GetDatabase(databaseId);
            return database.Tables;
        }

        /// <summary>
        /// Get enumerable collection of user Views in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        public IEnumerable<View> GetUserViews(int databaseId)
        {
            Database database = GetDatabase(databaseId);

            IEnumerable<View> rezult = (from View view in database.Views
                                        where !view.IsSystemObject
                                        select view
                );

            return rezult;
        }

        /// <summary>
        /// Get enumerable collection of system Views in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        public IEnumerable<View> GetSystemViews(int databaseId)
        {
            Database database = GetDatabase(databaseId);

            IEnumerable<View> rezult = (from View view in database.Views
                                        where view.IsSystemObject
                                        select view
                );

            return rezult;
        }


        // ------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Get enumerable collection of user StoredProcedures in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        public IEnumerable<StoredProcedure> GetUserStoredProcedures(int databaseId)
        {
            Database database = GetDatabase(databaseId);

            IEnumerable<StoredProcedure> rezult = (from StoredProcedure storedProcedure in database.StoredProcedures
                                                   where !storedProcedure.IsSystemObject
                                                   select storedProcedure
                );

            return rezult;
        }

        /// <summary>
        /// Get enumerable collection of system StoredProcedures in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        public IEnumerable<StoredProcedure> GetSystemStoredProcedures(int databaseId)
        {
            Database database = GetDatabase(databaseId);

            IEnumerable<StoredProcedure> rezult = (from StoredProcedure storedProcedure in database.StoredProcedures
                                                   where storedProcedure.IsSystemObject
                                                   select storedProcedure
                );

            return rezult;
        }


        private StoredProcedureCollection GetStoredProceduresCollection(int databaseId)
        {
            Database database = GetDatabase(databaseId);
            return database.StoredProcedures;
        }


        public IEnumerable<ExtendedStoredProcedure> GetExtendedStoredProcedures(int databaseId)
        {
            return GetExtendedStoredProceduresCollection(databaseId).Cast<ExtendedStoredProcedure>();
        }


        private ExtendedStoredProcedureCollection GetExtendedStoredProceduresCollection(int databaseId)
        {
            Database database = GetDatabase(databaseId);
            return database.ExtendedStoredProcedures;
        }


        public IEnumerable<PartitionFunction> GetPartitionFunctions(int databaseId)
        {
            return GetPartitionFunctionsCollection(databaseId).Cast<PartitionFunction>();
        }

        private PartitionFunctionCollection GetPartitionFunctionsCollection(int databaseId)
        {
            Database database = GetDatabase(databaseId);
            return database.PartitionFunctions;
        }

        // ------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Get enumerable collection of user functions in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        public IEnumerable<UserDefinedFunction> GetUserFunctions(int databaseId)
        {
            IEnumerable<UserDefinedFunction> rezult =
                (from UserDefinedFunction userDefinedFunction in GetUserDefinedFunctionsCollection(databaseId)
                 where !userDefinedFunction.IsSystemObject
                 select userDefinedFunction
                    );

            return rezult;
        }

        /// <summary>
        /// Get enumerable collection of system functions in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        public IEnumerable<UserDefinedFunction> GetSystemFunctions(int databaseId)
        {
            IEnumerable<UserDefinedFunction> rezult =
                (from UserDefinedFunction userDefinedFunction in GetUserDefinedFunctionsCollection(databaseId)
                 where userDefinedFunction.IsSystemObject
                 select userDefinedFunction
                    );

            return rezult;
        }


        private UserDefinedFunctionCollection GetUserDefinedFunctionsCollection(int databaseId)
        {
            Database database = GetDatabase(databaseId);
            return database.UserDefinedFunctions;
        }


        // ===========================================================================================================

        // ------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Get Database by Id
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        public Database GetDatabase(int databaseId)
        {
            return MsSqlServer.Databases.ItemById(databaseId);
        }

        /// <summary>
        /// Get Table by Id in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="tableId"></param>
        /// <returns></returns>
        public Table GetTable(int databaseId, int tableId)
        {
            return GetDatabase(databaseId).Tables.ItemById(tableId);
        }

        /// <summary>
        /// Get View by Id in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="viewId"></param>
        /// <returns></returns>
        public View GetView(int databaseId, int viewId)
        {
            return GetDatabase(databaseId).Views.ItemById(viewId);
        }

        /// <summary>
        /// Get StoredProcedure by Id in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="procedurewId"></param>
        /// <returns></returns>
        public StoredProcedure GetStoredProcedure(int databaseId, int procedurewId)
        {
            return GetDatabase(databaseId).StoredProcedures.ItemById(procedurewId);
        }


        /// <summary>
        /// Get UserDefinedFunction by Id in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="userDefinedFunctionId"></param>
        /// <returns></returns>
        public UserDefinedFunction GetUserDefinedFunction(int databaseId, int userDefinedFunctionId)
        {
            return GetDatabase(databaseId).UserDefinedFunctions.ItemById(userDefinedFunctionId);
        }

        // *************************************************************************************


        public ExtendedStoredProcedure GetExtendedStoredProcedure(int databaseId, int extendedStoredProcedureId)
        {
            return GetDatabase(databaseId).ExtendedStoredProcedures.ItemById(extendedStoredProcedureId);
        }


        public PartitionFunction GetPartitionFunction(int databaseId, int partitionFunctionId)
        {
            return GetDatabase(databaseId).PartitionFunctions.ItemById(partitionFunctionId);
        }


        // ------------------------------------------------------------------------------------------------------------


        // ===========================================================================================================


        //--------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Delete Database by Id
        /// </summary>
        /// <param name="databaseId"></param>
        public void DeleteDatabase(int databaseId)
        {
            Database database = GetDatabase(databaseId);
            database.Drop();
            //База удаляется, и после этого на .Alter() вылетает ошибка
            //database.Alter();
        }

        // ------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Delete Table by Id in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="tableId"></param>
        public void DeleteTable(int databaseId, int tableId)
        {
            Table table = GetTable(databaseId, tableId);
            table.Drop();
            table.Alter();
        }

        // ------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Delete View by Id in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="viewId"></param>
        public void DeleteView(int databaseId, int viewId)
        {
            View view = GetView(databaseId, viewId);
            view.Drop();
            view.Alter();
        }

        // ------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Delete StoredProcedure by Id in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="storedProcedureId"></param>
        public void DeleteStoredProcedure(int databaseId, int storedProcedureId)
        {
            StoredProcedure procedure = GetStoredProcedure(databaseId, storedProcedureId);
            procedure.Drop();
            procedure.Alter();
        }


        // ------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Delete function by Id in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="userDefinedFunctionId"></param>
        public void DeleteUserDefinedFunction(int databaseId, int userDefinedFunctionId)
        {
            UserDefinedFunction function = GetUserDefinedFunction(databaseId, userDefinedFunctionId);
            function.Drop();
            function.Alter();
        }

        // ------------------------------------------------------------------------------------------------------------
        // ===========================================================================================================

        /// <summary>
        /// Execute SQL query with result in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="sqlQuery"></param>
        /// <returns></returns>
        public DataSet ExecuteQuery(int databaseId, string sqlQuery)
        {
            return MsSqlServer.Databases[databaseId].ExecuteWithResults(sqlQuery);
        }

        /// <summary>
        /// Execute SQL query with result in default Database
        /// </summary>
        /// <param name="sqlQuery"></param>
        /// <returns></returns>
        public DataSet ExecuteQuery(string sqlQuery)
        {
            return MsSqlServer.Databases[0].ExecuteWithResults(sqlQuery);
        }


        // ------------------------------------------------------------------------------------------------------------


        // ===========================================================================================================
        // ===========================================================================================================

        /// <summary>
        /// Set new name to Database
        /// </summary>
        /// <param name="database"></param>
        /// <param name="newDatabaseName"></param>
        private void RenameDatabase(Database database, string newDatabaseName)
        {
            database.Rename(newDatabaseName);
            database.Alter();
        }

        /// <summary>
        /// Set new name to Database (get Database by Id)
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="newDatabaseName"></param>
        public void RenameDatabase(int databaseId, string newDatabaseName)
        {
            Database database = GetDatabase(databaseId);
            RenameDatabase(database, newDatabaseName);
        }


        // ------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Set new name to Table
        /// </summary>
        /// <param name="table"></param>
        /// <param name="newTableName"></param>
        private void RenameTable(Table table, string newTableName)
        {
            table.Rename(newTableName);
            table.Alter();
        }


        public void RenameTable(int databaseId, int tableId, string newTableName)
        {
            Table table = GetTable(databaseId, tableId);
            RenameTable(table, newTableName);
        }

        // ------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Set new name to View
        /// </summary>
        /// <param name="view"></param>
        /// <param name="newViewName"></param>
        private void RenameView(View view, string newViewName)
        {
            view.Rename(newViewName);
            view.Alter();
        }

        /// <summary>
        /// Set new name to View by id in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="viewId"></param>
        /// <param name="newViewName"></param>
        public void RenameView(int databaseId, int viewId, string newViewName)
        {
            View view = GetView(databaseId, viewId);
            RenameView(view, newViewName);
        }

        // ------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Set new name to StoredProcedure
        /// </summary>
        /// <param name="procedure"></param>
        /// <param name="newStoredProcedureName"></param>
        private void RenameStoredProcedure(StoredProcedure procedure, string newStoredProcedureName)
        {
            procedure.Rename(newStoredProcedureName);
            procedure.Alter();
        }


        public void RenameStoredProcedure(int databaseId, int storedProcedureId, string newStoredProcedureName)
        {
            StoredProcedure procedure = GetStoredProcedure(databaseId, storedProcedureId);
            RenameStoredProcedure(procedure, newStoredProcedureName);
        }

        // ------------------------------------------------------------------------------------------------------------

        // ===========================================================================================================
        // ===========================================================================================================

        /// <summary>
        /// Get Column by Id in this table of this database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="tableId"></param>
        /// <param name="columnId"></param>
        /// <returns></returns>
        public Column GetTableColumn(int databaseId, int tableId, int columnId)
        {
            return GetTable(databaseId, tableId).Columns.ItemById(columnId);
        }

        public Column GetViewColumn(int databaseId, int viewId, int columnId)
        {
            return GetView(databaseId, viewId).Columns.ItemById(columnId);
        }


        public Index GetTableIndex(int databaseId, int tableId, int idexId)
        {
            return GetTable(databaseId, tableId).Indexes.ItemById(idexId);
        }


        public IEnumerable<Column> GetTableColumns(int databaseId, int tableId)
        {
            return GetTable(databaseId, tableId).Columns.Cast<Column>();
        }

        public IEnumerable<Column> GetViewsColumns(int databaseId, int viewId)
        {
            return GetView(databaseId, viewId).Columns.Cast<Column>();
        }


        public IEnumerable<object> GetTableForeignKeys(int databaseId, int tableId)
        {
            List<object> keys =
                GetTable(databaseId, tableId).ForeignKeys.Cast<ForeignKey>().ToList().Cast<object>().ToList();

            // Search for primary key
            foreach (Index index in GetTable(databaseId, tableId).Indexes)
            {
                if (index.IndexKeyType == IndexKeyType.DriPrimaryKey)
                {
                    keys.Add(index);
                }
            }

            return keys;
        }


        public IEnumerable<Trigger> GetTableTriggers(int databaseId, int tableId)
        {
            return GetTable(databaseId, tableId).Triggers.Cast<Trigger>();
        }

        public IEnumerable<Trigger> GetViewTriggers(int databaseId, int tableId)
        {
            return GetView(databaseId, tableId).Triggers.Cast<Trigger>();
        }

        // ------------------------------------------------------------

        public IEnumerable<Index> GetTableIndexes(int databaseId, int tableId)
        {
            Database database = GetDatabase(databaseId);
            return database.Tables.ItemById(tableId).Indexes.Cast<Index>();
        }

        public IEnumerable<Index> GetViewIndexes(int databaseId, int viewId)
        {
            Database database = GetDatabase(databaseId);
            return database.Views.ItemById(viewId).Indexes.Cast<Index>();
        }


        public Database CreateNewDatabase(string newDatabaseName)
        {
            Database myDatabase = new Database(MsSqlServer, newDatabaseName);
            myDatabase.Create();

            myDatabase.Alter();

            return myDatabase;
        }
    }
}
