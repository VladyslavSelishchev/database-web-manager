﻿using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.SqlServer.Management.Smo;
using DbWebManager.BLL;
using System.Text;
using System.Web;
using System.Web.Mvc;
using DbWebManager.Controllers;
using DbWebManager.Models;
using Moq;
using DbWebManager.DAL;

namespace DbWebManager.Tests
{
    [TestClass]
    public class LoginControllerTest
    {
        [TestMethod]
        public void IndexTest()
        {
            // Arrange
            LoginController controller = new LoginController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof (NullReferenceException))]
        public void ConnectTest()
        {
            // Arrange
            LoginController controller = new LoginController();
            MsSqlConnectionModel model = new MsSqlConnectionModel();
            model.ServerName = "WrongServerName";
            model.UseWindowsIntegratedSecurity = true;
            // Act
            ViewResult result = controller.Connect(model) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }

    [TestClass]
    public class MsSqlManagerTest
    {
        [TestMethod]
        public void GetDatabase_id2_test()
        {
            // arrange
            Database k = new Database();
            IMsSqlConnector msSqlConnector = Mock.Of<IMsSqlConnector>(d => d.GetDatabase(2) == k);
            // act
            var currentDatabase = msSqlConnector.GetDatabase(2);
            // assert
            Assert.AreEqual(currentDatabase, k);
        }

        [TestMethod]
        public void GetTable_id2_from_DB_id2_test()
        {
            // arrange
            Table t = new Table();
            IMsSqlConnector msSqlConnector = Mock.Of<IMsSqlConnector>(d => d.GetTable(2, 2) == t);
            // act
            var currentTable = msSqlConnector.GetTable(2, 2);
            // assert
            Assert.AreEqual(currentTable, t);
        }

        [TestMethod]
        public void GetView_id2_from_DB_id2_test()
        {
            // arrange
            View t = new View();
            IMsSqlConnector msSqlConnector = Mock.Of<IMsSqlConnector>(d => d.GetView(2, 2) == t);
            // act
            var currentView = msSqlConnector.GetView(2, 2);
            // assert
            Assert.AreEqual(currentView, t);
        }

        [TestMethod]
        public void GetStoredProcedure_id2_from_DB_id2_test()
        {
            // arrange
            StoredProcedure t = new StoredProcedure();
            IMsSqlConnector msSqlConnector = Mock.Of<IMsSqlConnector>(d => d.GetStoredProcedure(2, 2) == t);
            // act
            var currentStoredProcedure = msSqlConnector.GetStoredProcedure(2, 2);
            // assert
            Assert.AreEqual(currentStoredProcedure, t);
        }

        [TestMethod]
        public void GetUserDefinedFunction_id2_from_DB_id2_test()
        {
            // arrange
            UserDefinedFunction t = new UserDefinedFunction();
            IMsSqlConnector msSqlConnector = Mock.Of<IMsSqlConnector>(d => d.GetUserDefinedFunction(2, 2) == t);
            // act
            var currentUserDefinedFunction = msSqlConnector.GetUserDefinedFunction(2, 2);
            // assert
            Assert.AreEqual(currentUserDefinedFunction, t);
        }

        [TestMethod]
        public void DeleteDatabase_id2_test()
        {
            // arrange
            IMsSqlConnector msSqlConnector = Mock.Of<IMsSqlConnector>();
            msSqlConnector.DeleteDatabase(2);
            // act
            var currentDatabase = msSqlConnector.GetDatabase(2);
            // assert
            Assert.IsNull(currentDatabase);
        }

        [TestMethod]
        public void DeleteTable_id2_from_DB_id2_test()
        {
            // arrange
            IMsSqlConnector msSqlConnector = Mock.Of<IMsSqlConnector>();
            msSqlConnector.DeleteTable(2, 2);
            // act
            var currentTable = msSqlConnector.GetTable(2, 2);
            // assert
            Assert.IsNull(currentTable);
        }

        [TestMethod]
        public void DeleteView_id2_from_DB_id2_test()
        {
            // arrange
            IMsSqlConnector msSqlConnector = Mock.Of<IMsSqlConnector>();
            msSqlConnector.DeleteView(2, 2);
            // act
            var currentView = msSqlConnector.GetView(2, 2);
            // assert
            Assert.IsNull(currentView);
        }

        [TestMethod]
        public void DeleteStoredProcedure_id2_from_DB_id2_test()
        {
            // arrange
            IMsSqlConnector msSqlConnector = Mock.Of<IMsSqlConnector>();
            msSqlConnector.DeleteStoredProcedure(2, 2);
            // act
            var currentStoredProcedure = msSqlConnector.GetStoredProcedure(2, 2);
            // assert
            Assert.IsNull(currentStoredProcedure);
        }

        [TestMethod]
        public void DeleteUserDefinedFunction_id2_from_DB_id2_test()
        {
            // arrange
            IMsSqlConnector msSqlConnector = Mock.Of<IMsSqlConnector>();
            msSqlConnector.DeleteUserDefinedFunction(2, 2);
            // act
            var currentUserDefinedFunction = msSqlConnector.GetUserDefinedFunction(2, 2);
            // assert
            Assert.IsNull(currentUserDefinedFunction);
        }
    }

    [TestClass]
    public class WorkspaceDbConnectorTest
    {
        [TestMethod]
        public void ReadTableData_id2_from_DB_id2_test()
        {
            // arrange
            DataTable t = new DataTable("465123");
            IWorkspaceDbConnector workspaceDbConnector = Mock.Of<IWorkspaceDbConnector>(d => d.ReadTableData(2, 2) == t);
            // act
            var currentReadTableData = workspaceDbConnector.ReadTableData(2, 2);
            // assert
            Assert.AreEqual(currentReadTableData, t);
        }

        [TestMethod]
        public void ReadViewData_id2_from_DB_id2_test()
        {
            // arrange
            DataTable t = new DataTable();
            IWorkspaceDbConnector workspaceDbConnector = Mock.Of<IWorkspaceDbConnector>(d => d.ReadViewData(2, 2) == t);
            // act
            var currentReadViewData = workspaceDbConnector.ReadViewData(2, 2);
            // assert
            Assert.AreEqual(currentReadViewData, t);
        }
    }
}