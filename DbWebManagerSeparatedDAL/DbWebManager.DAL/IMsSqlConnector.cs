﻿using Microsoft.SqlServer.Management.Smo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbWebManager.DAL
{
    public interface IMsSqlConnector
    {
        Server MsSqlServer { get; }


        void TryConnectToSqlServer();

        /// <summary>
        /// Get Database by Id
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        Database GetDatabase(int databaseId);

        /// <summary>
        /// Get enumerable collection of system Databases
        /// </summary>
        /// <returns></returns>
        IEnumerable<Database> GetSystemDatabases();

        /// <summary>
        /// Get enumerable collection of user Databases
        /// </summary>
        /// <returns></returns>
        IEnumerable<Database> GetUserDatabases();

        /// <summary>
        /// Get enumerable collection of user Tables in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        IEnumerable<Table> GetUserTables(int databaseId);

        /// <summary>
        /// Get enumerable collection of system Tables in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        IEnumerable<Table> GetSystemTables(int databaseId);

        /// <summary>
        /// Get enumerable collection of user Views in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        IEnumerable<View> GetUserViews(int databaseId);

        /// <summary>
        /// Get enumerable collection of system Views in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        IEnumerable<View> GetSystemViews(int databaseId);

        /// <summary>
        /// Get enumerable collection of user StoredProcedures in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        IEnumerable<StoredProcedure> GetUserStoredProcedures(int databaseId);

        /// <summary>
        /// Get enumerable collection of system StoredProcedures in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        IEnumerable<StoredProcedure> GetSystemStoredProcedures(int databaseId);

        /// <summary>
        /// Get enumerable collection of user functions in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        IEnumerable<UserDefinedFunction> GetUserFunctions(int databaseId);

        /// <summary>
        /// Get enumerable collection of system functions in Database
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        IEnumerable<UserDefinedFunction> GetSystemFunctions(int databaseId);


        /// <summary>
        /// Get Table by Id and Database Id
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="tableId"></param>
        /// <returns></returns>
        Table GetTable(int databaseId, int tableId);

        /// <summary>
        /// Get View by Id and Database Id
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="viewId"></param>
        /// <returns></returns>
        View GetView(int databaseId, int viewId);

        /// <summary>
        /// Get StoredProcedure by Id and Database Id
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="procedurewId"></param>
        /// <returns></returns>
        StoredProcedure GetStoredProcedure(int databaseId, int procedurewId);

        /// <summary>
        /// Get UserDefinedFunction by Id and Database Id
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="userDefinedFunctionId"></param>
        /// <returns></returns>
        UserDefinedFunction GetUserDefinedFunction(int databaseId, int userDefinedFunctionId);

        /// <summary>
        /// Delete Database by Id
        /// </summary>
        /// <param name="databaseId"></param>
        void DeleteDatabase(int databaseId);

        /// <summary>
        /// Delete Table by Id and Database Id
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="tableId"></param>
        void DeleteTable(int databaseId, int tableId);

        /// <summary>
        /// Delete View by Id and Database Id
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="viewId"></param>
        void DeleteView(int databaseId, int viewId);

        /// <summary>
        /// Delete StoredProcedure by Id and Database Id
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="storedProcedureId"></param>
        void DeleteStoredProcedure(int databaseId, int storedProcedureId);

        /// <summary>
        /// Delete UserDefinedFunction by Id and Database Id
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="userDefinedFunctionId"></param>
        void DeleteUserDefinedFunction(int databaseId, int userDefinedFunctionId);

        /// <summary>
        /// Get enumerable collection of Columns in Table 
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="tableId"></param>
        /// <returns></returns>
        IEnumerable<Column> GetTableColumns(int databaseId, int tableId);

        /// <summary>
        /// Get enumerable collection of Columns in View
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="viewId"></param>
        /// <returns></returns>
        IEnumerable<Column> GetViewsColumns(int databaseId, int viewId);

        /// <summary>
        /// Get enumerable collection of keys in Table
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="tableId"></param>
        /// <returns></returns>
        IEnumerable<object> GetTableForeignKeys(int databaseId, int tableId);

        /// <summary>
        /// Get enumerable collection of Triggers in Table
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="tableId"></param>
        /// <returns></returns>
        IEnumerable<Trigger> GetTableTriggers(int databaseId, int tableId);

        /// <summary>
        ///  Get enumerable collection of Triggers in View
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="tableId"></param>
        /// <returns></returns>
        IEnumerable<Trigger> GetViewTriggers(int databaseId, int tableId);

        /// <summary>
        ///  Get enumerable collection of Indexes in Table
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="tableId"></param>
        /// <returns></returns>
        IEnumerable<Index> GetTableIndexes(int databaseId, int tableId);

        /// <summary>
        /// Get enumerable collection of Indexes in View
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="viewId"></param>
        /// <returns></returns>
        IEnumerable<Index> GetViewIndexes(int databaseId, int viewId);

        /// <summary>
        /// Run SQL query on Database in MS SQL Server
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="sqlQuery"></param>
        /// <returns></returns>
        DataSet ExecuteQuery(int databaseId, string sqlQuery);

        /// <summary>
        /// Run SQL query in MS SQL Server
        /// </summary>
        /// <param name="sqlQuery"></param>
        /// <returns></returns>
        DataSet ExecuteQuery(string sqlQuery);
    }
}
