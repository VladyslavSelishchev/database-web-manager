﻿using System.Collections.Generic;


namespace DbWebManager.BLL
{
    public interface ITreeDbConnector
    {
        /// <summary>
        /// Get dictionary with Names and Id's of User databases
        /// </summary>
        /// <returns></returns>
        Dictionary<string, string> GetUserDatabasesDictionary();

        /// <summary>
        /// Get dictionary with Names and Id's of System databases
        /// </summary>
        /// <returns></returns>
        Dictionary<string, string> GetSystemDatabasesDictionary();


        // -----------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Get dictionary with Names and Id's of User tables
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        Dictionary<string, string> GetUserTablesDictionary(int databaseId);

        /// <summary>
        /// Get dictionary with Names and Id's of System tables
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        Dictionary<string, string> GetSystemTablesDictionary(int databaseId);

        // -----------------------------------------------------------------------------------------------------------
        /// <summary>
        ///  Get dictionary with Names and Id's of User views
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        Dictionary<string, string> GetUserViewsDictionary(int databaseId);

        /// <summary>
        /// Get dictionary with Names and Id's of System views
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        Dictionary<string, string> GetSystemViewsDictionary(int databaseId);

        // -----------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Get dictionary with Names and Id's of User stored procedures
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        Dictionary<string, string> GetUserStoredProceduresDictionary(int databaseId);

        /// <summary>
        /// Get dictionary with Names and Id's of System stored procedures
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        Dictionary<string, string> GetSystemStoredProceduresDictionary(int databaseId);

        // -----------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Get dictionary with Names and Id's of User functions
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        Dictionary<string, string> GetUserFunctionsDictionary(int databaseId);

        /// <summary>
        /// Get dictionary with Names and Id's of System functions
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        Dictionary<string, string> GetSystemFunctionsDictionary(int databaseId);

        // -----------------------------------------------------------------------------------------------------------
        Dictionary<string, string> GetSystemTableValuedFunctionsDictionary(int databaseId);

        // Scalar-valued Functions
        Dictionary<string, string> GetSystemScalarValuedFunctionsDictionary(int databaseId);


        // -----------------------------------------------------------------------------------------------------------

        // ==========================================================================================================================

        /// <summary>
        /// Get dictionary with Names and Id's of Columns in table
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="tableId"></param>
        /// <returns></returns>
        Dictionary<string, string> GetTableColumnsDictionary(int databaseId, int tableId);

        /// <summary>
        /// Get dictionary with Names and Id's of Columns in View
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="viewId"></param>
        /// <returns></returns>
        Dictionary<string, string> GetViewColumnsDictionary(int databaseId, int viewId);

        // -----------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Get dictionary with Names and Id's of Keys in table
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="tableId"></param>
        /// <returns></returns>
        Dictionary<string, string> GetTableForeignKeysDictionary(int databaseId, int tableId);

        // -----------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Get dictionary with Names and Id's of Triggers in table
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="tableId"></param>
        /// <returns></returns>
        Dictionary<string, string> GetTableTriggersDictionary(int databaseId, int tableId);

        /// <summary>
        /// Get dictionary with Names and Id's of Triggers in view
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="viewId"></param>
        /// <returns></returns>
        Dictionary<string, string> GetViewTriggersDictionary(int databaseId, int viewId);


        // -----------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Get dictionary with Names and Id's of Indexes in table
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="tableId"></param>
        /// <returns></returns>
        Dictionary<string, string> GetTableIndexesDictionary(int databaseId, int tableId);

        /// <summary>
        ///  Get dictionary with Names and Id's of Indexes in view
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="viewId"></param>
        /// <returns></returns>
        Dictionary<string, string> GetViewIndexesDictionary(int databaseId, int viewId);
    }
}