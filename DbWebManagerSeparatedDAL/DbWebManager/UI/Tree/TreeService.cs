﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using DbWebManager.BLL;
using DbWebManager.DAL;
using Ext.Net;
using Ext.Net.MVC;
using DbWebManager.Models;

namespace DbWebManager.UI
{
    public class TreeService : ITreeService
    {
        private ITreeDbConnector TreeDbConnector { get; set; }


        public TreeService(MsSqlConnectionModel connectionModel)
        {
            this.TreeDbConnector = new TreeDbConnector(connectionModel);
        }

        /// <summary>
        /// Get Nodes in StoreResult by parent node ID
        /// </summary>
        /// <param name="node">Parent node ID</param>
        /// <returns>Nodes collection</returns>
        public StoreResult GetNode(string node)
        {
            NameValueCollection parameters = HttpUtility.ParseQueryString(node);

            if (parameters.AllKeys.Contains("Folder"))
            {
                switch (parameters["Folder"])
                {
                    case "SystemDatabases":
                        return new StoreResult(GetSystemDatabasesNodesCollection(parameters.ToString()));
                    case "UserDatabases":
                        return new StoreResult(GetUserDatabasesNodesCollection(parameters.ToString()));
                }
            }

            if (parameters.AllKeys.Contains("Database"))
            {
                if (parameters.AllKeys.Contains("Folder"))
                {
                    if (parameters["Folder"] == "Tables")
                    {
                        return new StoreResult(GetTablesNodesCollection(parameters.ToString()));
                    }
                    if (parameters["Folder"] == "User Tables")
                    {
                        return new StoreResult(GetUserTablesNodesCollection(parameters.ToString()));
                    }
                    if (parameters["Folder"] == "System Tables")
                    {
                        return new StoreResult(GetSystemTablesNodesCollection(parameters.ToString()));
                    }
                    if (parameters["Folder"] == "Views")
                    {
                        return new StoreResult(GetViewsNodesCollection(parameters.ToString()));
                    }
                    if (parameters["Folder"] == "User Views")
                    {
                        return new StoreResult(GetUserViewsNodesCollection(parameters.ToString()));
                    }
                    if (parameters["Folder"] == "System Views")
                    {
                        return new StoreResult(GetSystemViewsNodesCollection(parameters.ToString()));
                    }
                }

                if (parameters["Folder"] == "Columns")
                {
                    return new StoreResult(GetColumnsNodesCollection(parameters.ToString()));
                }
                if (parameters["Folder"] == "Keys")
                {
                    return new StoreResult(GetTableForeignKeysNodesCollection(parameters.ToString()));
                }
                if (parameters["Folder"] == "Indexes")
                {
                    return new StoreResult(GetIndexesNodesCollection(parameters.ToString()));
                }
                if (parameters["Folder"] == "Programmability")
                {
                    return new StoreResult(GetProgrammabilityNodesCollection(parameters.ToString()));
                }

                // -------------------------------------------------------------------------------------------------------

                if (parameters["Folder"] == "Stored Procedures")
                {
                    return new StoreResult(GetStoredProceduresNodesCollection(parameters.ToString()));
                }
                if (parameters["Folder"] == "User Stored Procedures")
                {
                    return new StoreResult(GetUserStoredProceduresNodesCollection(parameters.ToString()));
                }
                if (parameters["Folder"] == "System Stored Procedures")
                {
                    return new StoreResult(GetSystemStoredProceduresNodesCollection(parameters.ToString()));
                }

                // -------------------------------------------------------------------------------------------------------

                if (parameters["Folder"] == "Functions")
                {
                    return new StoreResult(GetFunctionsNodesCollection(parameters.ToString()));
                }
                if (parameters["Folder"] == "User Functions")
                {
                    return new StoreResult(GetUserFunctionsNodesCollection(parameters.ToString()));
                }
                if (parameters["Folder"] == "System Functions")
                {
                    return new StoreResult(GetSytemFunctionsNodesCollection(parameters.ToString()));
                }

                // -------------------------------------------------------------------------------------------------------

                if (parameters.AllKeys.Contains("Table"))
                {
                    return new StoreResult(GetTableFolderNodesCollection(parameters.ToString()));
                }
                if (parameters.AllKeys.Contains("View"))
                {
                    return new StoreResult(GetViewFolderNodesCollection(parameters.ToString()));
                }

                return new StoreResult(GetDatabaseFolderNodesCollection(parameters.ToString()));
            }

            return new StoreResult();
        }


        /// <summary>
        /// Get object id in MSSQL Server
        /// </summary>
        /// <param name="parameters">string with input parameters</param>
        /// <param name="objName"></param>
        /// <returns></returns>
        public static int GetObjectIdFromParameters(string parameters, string objName)
        {
            NameValueCollection parametersCollection = HttpUtility.ParseQueryString(parameters);

            string[] tableValues = parametersCollection.GetValues(objName);
            if (tableValues == null || tableValues.Count() != 1 || string.IsNullOrWhiteSpace(tableValues[0].ToString()))
            {
                throw new ArgumentException(parameters);
            }
            int tableId = Convert.ToInt32(tableValues[0]);

            return tableId;
        }


        /// <summary>
        /// Delete key from parameters string by key Name
        /// </summary>
        /// <param name="parameters">string with input parameters</param>
        /// <param name="keyName"></param>
        /// <returns></returns>
        public static string DeleteKeyFromParameters(string parameters, string keyName)
        {
            NameValueCollection parametersCollection = HttpUtility.ParseQueryString(parameters);
            parametersCollection.Remove(keyName);

            return parametersCollection.ToString();
        }

        /// <summary>
        /// Check parameters for a kay presence
        /// </summary>
        /// <param name="parameters">string with input parameters</param>
        /// <param name="key"></param>
        /// <returns>has key or not</returns>
        public static bool ParametersHasKey(string parameters, string key)
        {
            NameValueCollection parametersCollection = HttpUtility.ParseQueryString(parameters);
            return parametersCollection.AllKeys.Contains(key);
        }

        // ---------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Create new Node for TreePanel
        /// </summary>
        /// <param name="nodeId"></param>
        /// <param name="nodeText"></param>
        /// <param name="nodeIcon"></param>
        /// <param name="nodeType"></param>
        /// <param name="expandable"></param>
        /// <returns>Node</returns>
        private Node CreateNode(string nodeId, string nodeText, Icon nodeIcon, string nodeType, bool expandable = true)
        {
            Node node = new Node();
            node.NodeID = nodeId;
            node.Text = nodeText;
            node.Icon = nodeIcon;
            node.AttributesObject = new {nodeType = nodeType};
            node.CustomAttributes.Add(new ConfigItem("nodeType", nodeType));
            node.Expandable = expandable;
            return node;
        }

        /// <summary>
        /// Create new node for TreePanel presenting Folder
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="folderId"></param>
        /// <param name="folderName"></param>
        /// <param name="icon"></param>
        /// <returns></returns>
        private Node CreateFolderNode(string parameters, string folderId, string folderName, Icon icon = Icon.Table)
        {
            NameValueCollection parametersCollection = HttpUtility.ParseQueryString(parameters);
            parametersCollection.Add(folderId, folderName);
            return CreateNode(parametersCollection.ToString(), folderName, icon, "Folder");
        }

        /// <summary>
        /// Create new collection of nodes
        /// </summary>
        /// <param name="typeName"></param>
        /// <param name="namesDictionary"></param>
        /// <param name="parameters"></param>
        /// <param name="icon"></param>
        /// <param name="expandable"></param>
        /// <returns></returns>
        private NodeCollection CreateNodeCollection(string typeName, Dictionary<string, string> namesDictionary,
            string parameters, Icon icon,
            bool expandable = true)
        {
            NodeCollection nodes = new NodeCollection();

            foreach (KeyValuePair<string, string> variable in namesDictionary)
            {
                NameValueCollection parametersCollection = HttpUtility.ParseQueryString(parameters);
                parametersCollection.Add(typeName, variable.Key);

                nodes.Add(CreateNode(parametersCollection.ToString(), variable.Value, icon, typeName, expandable));
            }

            return nodes;
        }

        // -------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Create new collection of children nodes for Table node
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private NodeCollection GetTablesNodesCollection(string parameters)
        {
            NodeCollection nodes = new NodeCollection();

            parameters = DeleteKeyFromParameters(parameters, "Folder");

            nodes.Add(CreateFolderNode(parameters, "Folder", "User Tables"));
            nodes.Add(CreateFolderNode(parameters, "Folder", "System Tables"));

            return nodes;
        }

        /// <summary>
        /// Create new collection of children nodes for "User Tables" node
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private NodeCollection GetUserTablesNodesCollection(string parameters)
        {
            parameters = DeleteKeyFromParameters(parameters, "Folder");
            int databaseId = GetObjectIdFromParameters(parameters, "Database");

            var tables = TreeDbConnector.GetUserTablesDictionary(databaseId); // !D!

            return CreateNodeCollection("Table", tables, parameters, Icon.Table);
        }

        /// <summary>
        /// Create new collection of children nodes for "System Tables" node
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private NodeCollection GetSystemTablesNodesCollection(string parameters)
        {
            parameters = DeleteKeyFromParameters(parameters, "Folder");
            int databaseId = GetObjectIdFromParameters(parameters, "Database");

            var tables = TreeDbConnector.GetSystemTablesDictionary(databaseId); // !D!

            return CreateNodeCollection("Table", tables, parameters, Icon.Table);
        }

        /// <summary>
        /// Create new collection of children nodes for View node
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private NodeCollection GetViewsNodesCollection(string parameters)
        {
            NodeCollection nodes = new NodeCollection();

            parameters = DeleteKeyFromParameters(parameters, "Folder");

            nodes.Add(CreateFolderNode(parameters, "Folder", "User Views", Icon.ApplicationViewList));
            nodes.Add(CreateFolderNode(parameters, "Folder", "System Views", Icon.ApplicationViewList));

            return nodes;
        }

        /// <summary>
        /// Create new collection of children nodes for "User Views" node
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private NodeCollection GetUserViewsNodesCollection(string parameters)
        {
            parameters = DeleteKeyFromParameters(parameters, "Folder");
            int databaseId = GetObjectIdFromParameters(parameters, "Database");

            var views = TreeDbConnector.GetUserViewsDictionary(databaseId); // !D!

            return CreateNodeCollection("View", views, parameters, Icon.ApplicationViewList);
        }

        /// <summary>
        /// Create new collection of children nodes for "System Views" node
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private NodeCollection GetSystemViewsNodesCollection(string parameters)
        {
            parameters = DeleteKeyFromParameters(parameters, "Folder");
            int databaseId = GetObjectIdFromParameters(parameters, "Database");

            var views = TreeDbConnector.GetSystemViewsDictionary(databaseId); // !D!

            return CreateNodeCollection("View", views, parameters, Icon.ApplicationViewList);
        }

        /// <summary>
        /// Create new collection of children nodes for "Columns" node
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private NodeCollection GetColumnsNodesCollection(string parameters)
        {
            NodeCollection nodes = new NodeCollection();
            int databaseId = GetObjectIdFromParameters(parameters, "Database");
            parameters = DeleteKeyFromParameters(parameters, "Folder");

            if (ParametersHasKey(parameters, "Table") == true)
            {
                int tableId = GetObjectIdFromParameters(parameters, "Table");
                var columns = TreeDbConnector.GetTableColumnsDictionary(databaseId, tableId);
                nodes = CreateNodeCollection("Column", columns, parameters, Icon.TableColumn, false);
            }
            if (ParametersHasKey(parameters, "View") == true)
            {
                int viewId = GetObjectIdFromParameters(parameters, "View");
                var columns = TreeDbConnector.GetViewColumnsDictionary(databaseId, viewId);
                nodes = CreateNodeCollection("Column", columns, parameters, Icon.TableColumn, false);
            }

            return nodes;
        }

        /// <summary>
        /// Create new collection of children nodes for "Keys" node
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private NodeCollection GetTableForeignKeysNodesCollection(string parameters)
        {
            parameters = DeleteKeyFromParameters(parameters, "Folder");
            int databaseId = GetObjectIdFromParameters(parameters, "Database");
            int tableId = GetObjectIdFromParameters(parameters, "Table");

            var keys = TreeDbConnector.GetTableForeignKeysDictionary(databaseId, tableId);

            return CreateNodeCollection("ForeignKey", keys, parameters, Icon.Key, false);
        }

        /// <summary>
        /// Create new collection of children nodes for "Indexes" node
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private NodeCollection GetIndexesNodesCollection(string parameters)
        {
            NodeCollection nodes = new NodeCollection();
            parameters = DeleteKeyFromParameters(parameters, "Folder");
            int databaseId = GetObjectIdFromParameters(parameters, "Database");

            if (ParametersHasKey(parameters, "Table") == true)
            {
                int tableId = GetObjectIdFromParameters(parameters, "Table");
                var indexes = TreeDbConnector.GetTableIndexesDictionary(databaseId, tableId);
                nodes = CreateNodeCollection("Index", indexes, parameters, Icon.TextIndent, false);
            }
            if (ParametersHasKey(parameters, "View") == true)
            {
                int viewId = GetObjectIdFromParameters(parameters, "View");
                var indexes = TreeDbConnector.GetViewIndexesDictionary(databaseId, viewId);
                nodes = CreateNodeCollection("Index", indexes, parameters, Icon.TextIndent, false);
            }

            return nodes;
        }

        /// <summary>
        /// Create new collection of children nodes for "Programmability" node
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private NodeCollection GetProgrammabilityNodesCollection(string parameters)
        {
            NodeCollection nodes = new NodeCollection();

            parameters = DeleteKeyFromParameters(parameters, "Folder");

            nodes.Add(CreateFolderNode(parameters, "Folder", "Stored Procedures", Icon.Package));
            nodes.Add(CreateFolderNode(parameters, "Folder", "Functions", Icon.Basket));

            return nodes;
        }

        /// <summary>
        /// Create new collection of children nodes for "StoredProcedures" node
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private NodeCollection GetStoredProceduresNodesCollection(string parameters)
        {
            NodeCollection nodes = new NodeCollection();

            parameters = DeleteKeyFromParameters(parameters, "Folder");

            nodes.Add(CreateFolderNode(parameters, "Folder", "User Stored Procedures", Icon.Package));
            nodes.Add(CreateFolderNode(parameters, "Folder", "System Stored Procedures", Icon.Package));

            return nodes;
        }

        /// <summary>
        /// Create new collection of children nodes for "User Stored Procedures" node
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private NodeCollection GetUserStoredProceduresNodesCollection(string parameters)
        {
            NodeCollection nodes = new NodeCollection();
            parameters = DeleteKeyFromParameters(parameters, "Folder");
            int databaseId = GetObjectIdFromParameters(parameters, "Database");

            var userStoredProcedures = TreeDbConnector.GetUserStoredProceduresDictionary(databaseId);

            nodes.AddRange(CreateNodeCollection("StoredProcedure", userStoredProcedures, parameters,
                Icon.Package,
                false));

            return nodes;
        }

        /// <summary>
        /// Create new collection of children nodes for "System Stored Procedures" node
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private NodeCollection GetSystemStoredProceduresNodesCollection(string parameters)
        {
            parameters = DeleteKeyFromParameters(parameters, "Folder");
            int databaseId = GetObjectIdFromParameters(parameters, "Database");

            var systemStoredProcedures = TreeDbConnector.GetSystemStoredProceduresDictionary(databaseId);

            return CreateNodeCollection("StoredProcedure", systemStoredProcedures, parameters,
                Icon.Package, false);
        }

        /// <summary>
        /// Create new collection of children nodes for "Functions" node
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private NodeCollection GetFunctionsNodesCollection(string parameters)
        {
            NodeCollection nodes = new NodeCollection();
            parameters = DeleteKeyFromParameters(parameters, "Folder");

            nodes.Add(CreateFolderNode(parameters, "Folder", "User Functions", Icon.Basket));
            nodes.Add(CreateFolderNode(parameters, "Folder", "System Functions", Icon.Basket));

            return nodes;
        }

        /// <summary>
        /// Create new collection of children nodes for "System Functions" node
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private NodeCollection GetSytemFunctionsNodesCollection(string parameters)
        {
            parameters = DeleteKeyFromParameters(parameters, "Folder");
            int databaseId = GetObjectIdFromParameters(parameters, "Database");

            var result = TreeDbConnector.GetSystemFunctionsDictionary(databaseId);

            return CreateNodeCollection("UserDefinedFunction", result, parameters, Icon.Basket, false);
        }

        /// <summary>
        /// Create new collection of children nodes for "User Functions" node
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private NodeCollection GetUserFunctionsNodesCollection(string parameters)
        {
            parameters = DeleteKeyFromParameters(parameters, "Folder");
            int databaseId = GetObjectIdFromParameters(parameters, "Database");

            var userDefinedFunctions = TreeDbConnector.GetUserFunctionsDictionary(databaseId);

            return CreateNodeCollection("UserDefinedFunction", userDefinedFunctions, parameters, Icon.Basket,
                false);
        }

        /// <summary>
        /// Create new collection of children nodes for current Database node
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private NodeCollection GetDatabaseFolderNodesCollection(string parameters)
        {
            NodeCollection nodes = new NodeCollection();

            nodes.Add(CreateFolderNode(parameters, "Folder", "Tables"));
            nodes.Add(CreateFolderNode(parameters, "Folder", "Views", Icon.ApplicationViewList));
            nodes.Add(CreateFolderNode(parameters, "Folder", "Programmability", Icon.ApplicationForm));

            return nodes;
        }

        /// <summary>
        /// Create new collection of children nodes for current Table node
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private NodeCollection GetTableFolderNodesCollection(string parameters)
        {
            NodeCollection nodes = new NodeCollection();

            nodes.Add(CreateFolderNode(parameters, "Folder", "Columns", Icon.TableColumn));
            nodes.Add(CreateFolderNode(parameters, "Folder", "Keys", Icon.Key));
            nodes.Add(CreateFolderNode(parameters, "Folder", "Indexes", Icon.TextIndent));

            return nodes;
        }

        /// <summary>
        /// Create new collection of children nodes for current View node
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private NodeCollection GetViewFolderNodesCollection(string parameters)
        {
            NodeCollection nodes = new NodeCollection();

            nodes.Add(CreateFolderNode(parameters, "Folder", "Columns", Icon.TableColumn));
            nodes.Add(CreateFolderNode(parameters, "Folder", "Indexes", Icon.TextIndent));

            return nodes;
        }

        /// <summary>
        /// Create new collection of children nodes for "System Databases" node
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private NodeCollection GetSystemDatabasesNodesCollection(string parameters)
        {
            parameters = DeleteKeyFromParameters(parameters, "Folder");
            var databases = TreeDbConnector.GetSystemDatabasesDictionary();
            return GetDatabasesNodesCollection(parameters, databases);
        }

        /// <summary>
        /// Create new collection of children nodes for "User Databases" node
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private NodeCollection GetUserDatabasesNodesCollection(string parameters)
        {
            parameters = DeleteKeyFromParameters(parameters, "Folder");
            var databases = TreeDbConnector.GetUserDatabasesDictionary();
            return GetDatabasesNodesCollection(parameters, databases);
        }

        /// <summary>
        /// Create new collection of children nodes for "System Databases" or "User Databases" node
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="databases"></param>
        /// <returns></returns>
        private NodeCollection GetDatabasesNodesCollection(string parameters, Dictionary<string, string> databases)
        {
            NodeCollection nodes = new NodeCollection();
            foreach (KeyValuePair<string, string> database in databases)
            {
                NameValueCollection databasesParametersCollection = HttpUtility.ParseQueryString(parameters);
                databasesParametersCollection.Add("Database", database.Key);

                nodes.Add(CreateNode(databasesParametersCollection.ToString(), database.Value, Icon.Database,
                    "Database"));
            }

            return nodes;
        }
    }
}