﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using DbWebManager.BLL;
using DbWebManager.Models;
using Ext.Net;
using DbWebManager.DAL;


namespace DbWebManager.UI
{
    public class WorkspaceService : IWorkspaceService
    {
        private IWorkspaceDbConnector Connector { get; set; }


        public WorkspaceService(MsSqlConnectionModel connectionModel)
        {
            IWorkspaceDbConnector workspaceDbConnector = new WorkspaceDbConnector(connectionModel);
            Connector = workspaceDbConnector;
        }


        public void TryConnect()
        {
            Connector.TryConnect();
        }


        /// <summary>
        /// Get new GridPanel with Data from table in new Panel
        /// </summary>
        /// <param name="table">table with data</param>
        /// <param name="parameters">object id</param>
        /// <param name="buttons">List of buttons for Toolbar</param>
        /// <returns></returns>
        private Panel GetPanelFromDataTable(DataTable table, string parameters, IEnumerable<Button> buttons,
            Icon panelIcon = Icon.None)
        {
            parameters = TreeService.DeleteKeyFromParameters(parameters, "Root");

            List<ModelField> fields = new ItemsCollection<ModelField>();
            List<ColumnBase> columns = new List<ColumnBase>();

            columns.Add(Html.X().RowNumbererColumn());

            foreach (DataColumn column in table.Columns)
            {
                fields.Add(new ModelField(column.ColumnName));
                Ext.Net.Column extNetColumn = new Ext.Net.Column();
                extNetColumn.Text = column.ColumnName;
                extNetColumn.DataIndex = column.ColumnName;
                extNetColumn.Flex = 1;
                columns.Add(extNetColumn);
            }

            Panel newPanel = new Panel()
            {
                ID = parameters.Replace("-", ""),
                Icon = panelIcon,
                Title = table.TableName,
                Closable = true,
                AutoScroll = true,
                BodyPadding = 6,
                Items =
                {
                    Html.X().Toolbar()
                        .Items(buttons),
                    Html.X().GridPanel()
                        .Store(
                            Html.X().Store()
                                .PageSize(20)
                                .Model(Html.X().Model()
                                    .IDProperty("ID")
                                    .Fields(fields)
                                )
                                .DataSource(table)
                        )
                        .ColumnModel(columns)
                        .SortableColumns(true)
                        .View(
                            Html.X().GridView()
                                .StripeRows(true)
                        )
                        .SelectionModel(Html.X().CellSelectionModel())
                        .BottomBar(
                            Html.X().PagingToolbar()
                                .HideRefresh(true)
                        )
                        .WidthSpec("97%")
                        .Margin(0)
                        .Padding(0)
                }
            };

            return newPanel;
        }

        /// <summary>
        /// Gets Panel with different data depending on the input object id and type
        /// </summary>
        /// <param name="node">object id</param>
        /// <param name="nodeType">object type</param>
        /// <returns></returns>
        public Panel GetPanel(string node, string nodeType)
        {
            NameValueCollection parameters = HttpUtility.ParseQueryString(node);

            if (nodeType == "Folder")
            {
                switch (parameters["Folder"])
                {
                    case "SystemDatabases":
                        return ReadSystemDatabasesToPanel(parameters.ToString());
                    case "UserDatabases":
                        return ReadUserDatabasesToPanel(parameters.ToString());
                    case "System Tables":
                        return ReadSystemTablesToPanel(parameters.ToString());
                    case "User Tables":
                        return ReadUserTablesToPanel(parameters.ToString());
                    case "System Views":
                        return ReadSystemViewsToPanel(parameters.ToString());
                    case "User Views":
                        return ReadUserViewsToPanel(parameters.ToString());
                    case "System Stored Procedures":
                        return ReadSystemStoredProceduresToPanel(parameters.ToString());
                    case "User Stored Procedures":
                        return ReadUserStoredProceduresToPanel(parameters.ToString());
                    case "User Functions":
                        return ReadUserFunctionsToPanel(parameters.ToString());
                    case "System Functions":
                        return ReadSystemFunctionsToPanel(parameters.ToString());
                    case "serverInfoPanel":
                        return GetServerInfoPanel();
                    case "Keys":
                        return ReadKeysToPanel(parameters.ToString());
                    case "Indexes":
                        return ReadIndexesToPanel(parameters.ToString());
                }
            }

            if (parameters.AllKeys.Contains("Table") && nodeType == "Folder")
            {
                if (parameters["Folder"] == "Columns")
                {
                    return ReadTableColumnsToPanel(parameters.ToString());
                }
            }
            if (parameters.AllKeys.Contains("View") && nodeType == "Folder")
            {
                if (parameters["Folder"] == "Columns")
                {
                    return ReadViewColumnsToPanel(parameters.ToString());
                }
            }
            if (parameters.AllKeys.Contains("Table") && nodeType == "Table")
            {
                return ReadTableDataToPanel(parameters.ToString());
            }
            if (parameters.AllKeys.Contains("View") && nodeType == "View")
            {
                return ReadViewDataToPanel(parameters.ToString());
            }
            if (parameters.AllKeys.Contains("StoredProcedure") &&
                nodeType == "StoredProcedure")
            {
                return GetStoredProcedureStructureToPanel(parameters.ToString());
            }
            // выводит и пользовательские и системные функции
            if (parameters.AllKeys.Contains("UserDefinedFunction") &&
                nodeType == "UserDefinedFunction")
            {
                return GetFunctionStructureToPanel(parameters.ToString());
            }

            return null;
        }

        private Panel ReadTableDataToPanel(string parameters)
        {
            int databaseId = TreeService.GetObjectIdFromParameters(parameters, "Database");
            ;
            int tableId = TreeService.GetObjectIdFromParameters(parameters, "Table");
            ;

            DataTable resultTable = Connector.ReadTableData(databaseId, tableId);

            return GetPanelFromDataTable(resultTable, parameters, GetButtonsForTable(parameters), Icon.Table);
        }

        /// <summary>
        /// Get Panel with List of System databases
        /// </summary>
        /// <param name="parameters">object id</param>
        /// <returns>Panel</returns>
        private Panel ReadSystemDatabasesToPanel(string parameters)
        {
            string tabName = "System Databases";
            return ReadDatabasesToPanel(parameters, Connector.SqlConnector.GetSystemDatabases(), tabName);
        }

        /// <summary>
        /// Get Panel with List of User databases
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private Panel ReadUserDatabasesToPanel(string parameters)
        {
            string tabName = "User Databases";
            return ReadDatabasesToPanel(parameters, Connector.SqlConnector.GetUserDatabases(), tabName);
        }

        /// <summary>
        /// Get Panel with List of System tables in one database
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private Panel ReadSystemTablesToPanel(string parameters)
        {
            int databaseId = TreeService.GetObjectIdFromParameters(parameters, "Database");
            string tabName = Connector.SqlConnector.GetDatabase(databaseId).Name + " - System Tables";
            return ReadTablesToPanel(parameters, Connector.SqlConnector.GetSystemTables(databaseId), tabName);
        }

        /// <summary>
        /// Get Panel with List of User tables in one database
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private Panel ReadUserTablesToPanel(string parameters)
        {
            int databaseId = TreeService.GetObjectIdFromParameters(parameters, "Database");
            string tabName = Connector.SqlConnector.GetDatabase(databaseId).Name + " - User Tables";
            return ReadTablesToPanel(parameters, Connector.SqlConnector.GetUserTables(databaseId), tabName);
        }

        /// <summary>
        /// Get Panel with List of System views in one database
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns>Panel</returns>
        private Panel ReadSystemViewsToPanel(string parameters)
        {
            int databaseId = TreeService.GetObjectIdFromParameters(parameters, "Database");
            string tabName = Connector.SqlConnector.GetDatabase(databaseId).Name + " - System Views";
            return ReadTablesToPanel(parameters, Connector.SqlConnector.GetSystemViews(databaseId), tabName);
        }

        /// <summary>
        /// Get Panel with List of User views in one database
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns>Panel</returns>
        private Panel ReadUserViewsToPanel(string parameters)
        {
            int databaseId = TreeService.GetObjectIdFromParameters(parameters, "Database");
            string tabName = Connector.SqlConnector.GetDatabase(databaseId).Name + " - User Views";
            return ReadTablesToPanel(parameters, Connector.SqlConnector.GetUserViews(databaseId), tabName);
        }

        /// <summary>
        /// Get Panel with List of System procedures in one database
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns>Panel</returns>
        private Panel ReadSystemStoredProceduresToPanel(string parameters)
        {
            int databaseId = TreeService.GetObjectIdFromParameters(parameters, "Database");
            string tabName = Connector.SqlConnector.GetDatabase(databaseId).Name + " - System Stored Procedures";
            return ReadTablesToPanel(parameters, Connector.SqlConnector.GetSystemStoredProcedures(databaseId), tabName);
        }

        /// <summary>
        /// Get Panel with List of User procedures in one database
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns>Panel</returns>
        private Panel ReadUserStoredProceduresToPanel(string parameters)
        {
            int databaseId = TreeService.GetObjectIdFromParameters(parameters, "Database");
            string tabName = Connector.SqlConnector.GetDatabase(databaseId).Name + " - User Stored Procedures";
            return ReadTablesToPanel(parameters, Connector.SqlConnector.GetUserStoredProcedures(databaseId), tabName);
        }

        private Panel ReadUserFunctionsToPanel(string parameters)
        {
            int databaseId = TreeService.GetObjectIdFromParameters(parameters, "Database");
            string tabName = Connector.SqlConnector.GetDatabase(databaseId).Name + " - User Functions";
            return ReadTablesToPanel(parameters, Connector.SqlConnector.GetUserFunctions(databaseId), tabName);
        }

        private Panel ReadSystemFunctionsToPanel(string parameters)
        {
            int databaseId = TreeService.GetObjectIdFromParameters(parameters, "Database");
            string tabName = Connector.SqlConnector.GetDatabase(databaseId).Name + " - User Functions";
            return ReadTablesToPanel(parameters, Connector.SqlConnector.GetSystemFunctions(databaseId), tabName);
        }

        private Panel ReadKeysToPanel(string parameters)
        {
            Panel panel = null;
            int databaseId = TreeService.GetObjectIdFromParameters(parameters, "Database");
            if (TreeService.ParametersHasKey(parameters, "Table") == true)
            {
                int tableId = TreeService.GetObjectIdFromParameters(parameters, "Table");
                string tabName = Connector.SqlConnector.GetDatabase(databaseId).Name +
                                 " - " + Connector.SqlConnector.GetTable(databaseId, tableId).Name + " - Keys";
                panel = ReadKeysToPanel(parameters, Connector.SqlConnector.GetTableForeignKeys(databaseId, tableId),
                    tabName);
            }
            if (TreeService.ParametersHasKey(parameters, "View") == true)
            {
                return null;
            }

            return panel;
        }

        private Panel ReadIndexesToPanel(string parameters)
        {
            int databaseId = TreeService.GetObjectIdFromParameters(parameters, "Database");
            Panel panel = null;
            if (TreeService.ParametersHasKey(parameters, "Table") == true)
            {
                int tableId = TreeService.GetObjectIdFromParameters(parameters, "Table");
                string tabName = Connector.SqlConnector.GetDatabase(databaseId).Name +
                                 " - " + Connector.SqlConnector.GetTable(databaseId, tableId).Name + " - Indexes";
                panel = ReadIndexesToPanel(parameters, Connector.SqlConnector.GetTableIndexes(databaseId, tableId),
                    tabName);
            }
            if (TreeService.ParametersHasKey(parameters, "View") == true)
            {
                int viewId = TreeService.GetObjectIdFromParameters(parameters, "View");
                string tabName = Connector.SqlConnector.GetDatabase(databaseId).Name +
                                 " - " + Connector.SqlConnector.GetView(databaseId, viewId).Name + " - Indexes";
                panel = ReadIndexesToPanel(parameters, Connector.SqlConnector.GetViewIndexes(databaseId, viewId),
                    tabName);
            }

            return panel;
        }

        private Panel ReadTableColumnsToPanel(string parameters)
        {
            int databaseId = TreeService.GetObjectIdFromParameters(parameters, "Database");
            int tableId = TreeService.GetObjectIdFromParameters(parameters, "Table");
            string tabName = Connector.SqlConnector.GetDatabase(databaseId).Name + " - " +
                             Connector.SqlConnector.GetTable(databaseId, tableId).Name + " - Columns";
            return ReadColumnsToPanel(parameters, Connector.SqlConnector.GetTableColumns(databaseId, tableId), tabName);
        }

        private Panel ReadViewColumnsToPanel(string parameters)
        {
            int databaseId = TreeService.GetObjectIdFromParameters(parameters, "Database");
            int viewId = TreeService.GetObjectIdFromParameters(parameters, "View");
            string tabName = Connector.SqlConnector.GetDatabase(databaseId).Name + " - " +
                             Connector.SqlConnector.GetView(databaseId, viewId).Name + " - Columns";
            return ReadColumnsToPanel(parameters, Connector.SqlConnector.GetViewsColumns(databaseId, viewId), tabName);
        }

        private Panel ReadColumnsToPanel(string parameters, IEnumerable<object> objects, string tabName)
        {
            DataTable resultTable = new DataTable();
            resultTable.TableName = tabName;

            DataColumn nameColumn = new DataColumn("Name", typeof (string));
            DataColumn dataTypeColumn = new DataColumn("DataType", typeof (string));
            DataColumn inPrimaryKeyColumn = new DataColumn("In Primary Key", typeof (bool));
            DataColumn isForeignKeyColumn = new DataColumn("Is Foreign Key", typeof (bool));
            DataColumn nullableColumn = new DataColumn("Is Nullable", typeof (bool));
            DataColumn idColumn = new DataColumn("ID", typeof (int));

            resultTable.Columns.Add(nameColumn);
            resultTable.Columns.Add(dataTypeColumn);
            resultTable.Columns.Add(inPrimaryKeyColumn);
            resultTable.Columns.Add(isForeignKeyColumn);
            resultTable.Columns.Add(nullableColumn);
            resultTable.Columns.Add(idColumn);

            foreach (dynamic obj in objects)
            {
                DataRow row = resultTable.NewRow();
                row[nameColumn] = obj.Name;
                row[dataTypeColumn] = obj.DataType + " (" + obj.DataType.MaximumLength + ")";
                row[inPrimaryKeyColumn] = obj.InPrimaryKey;
                row[isForeignKeyColumn] = obj.IsForeignKey;
                row[nullableColumn] = obj.Nullable;
                row[idColumn] = obj.ID;

                resultTable.Rows.Add(row);
            }

            List<Button> buttons = new List<Button>();

            return GetPanelFromDataTable(resultTable, parameters, buttons, Icon.TableColumn);
        }


        private Panel ReadTablesToPanel<T>(string parameters, IEnumerable<T> objects, string tabName)
        {
            DataTable resultTable = new DataTable();
            resultTable.TableName = tabName;

            DataColumn nameColumn = new DataColumn("Name", typeof (string));
            DataColumn schemaColumn = new DataColumn("Schema", typeof (string));
            DataColumn createDateColumn = new DataColumn("Created date", typeof (String));
            DataColumn dateLastModifiedColumn = new DataColumn("Last Modified Date", typeof (String));
            DataColumn idColumn = new DataColumn("ID", typeof (int));

            resultTable.Columns.Add(nameColumn);
            resultTable.Columns.Add(schemaColumn);
            resultTable.Columns.Add(createDateColumn);
            resultTable.Columns.Add(dateLastModifiedColumn);
            resultTable.Columns.Add(idColumn);

            foreach (dynamic obj in objects)
            {
                DataRow row = resultTable.NewRow();
                row[nameColumn] = obj.Name;
                row[schemaColumn] = obj.Schema;
                row[createDateColumn] =
                    ((DateTime) obj.CreateDate).ToString("dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                row[dateLastModifiedColumn] =
                    ((DateTime) obj.DateLastModified).ToString("dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                row[idColumn] = obj.ID;

                resultTable.Rows.Add(row);
            }

            List<Button> buttons = new List<Button>();
            buttons.Add(GetCreateButton(parameters, typeof (T).Name));

            Icon icon = Icon.None;
            if (typeof (T).Name == "Table")
            {
                icon = Icon.Table;
            }
            if (typeof (T).Name == "View")
            {
                icon = Icon.ApplicationViewList;
            }
            if (typeof (T).Name == "StoredProcedure")
            {
                icon = Icon.Package;
            }
            if (typeof (T).Name == "UserDefinedFunction")
            {
                icon = Icon.Basket;
            }
            if (typeof (T).Name == "ForeignKey")
            {
                icon = Icon.Key;
            }
            if (typeof (T).Name == "Index")
            {
                icon = Icon.TextIndent;
            }

            return GetPanelFromDataTable(resultTable, parameters, buttons, icon);
        }


        private Panel ReadKeysToPanel(string parameters, IEnumerable<object> objects, string tabName)
        {
            DataTable resultTable = new DataTable();
            resultTable.TableName = tabName;

            DataColumn nameColumn = new DataColumn("Name", typeof (string));
            DataColumn createDateColumn = new DataColumn("Created date", typeof (String));
            DataColumn dateLastModifiedColumn = new DataColumn("Last Modified Date", typeof (String));
            DataColumn idColumn = new DataColumn("ID", typeof (int));

            resultTable.Columns.Add(nameColumn);
            resultTable.Columns.Add(createDateColumn);
            resultTable.Columns.Add(dateLastModifiedColumn);
            resultTable.Columns.Add(idColumn);

            foreach (dynamic obj in objects)
            {
                DataRow row = resultTable.NewRow();

                if (obj is Microsoft.SqlServer.Management.Smo.Index)
                {
                    row[nameColumn] = obj.Name;
                    row[createDateColumn] = "";
                    row[dateLastModifiedColumn] = "";
                    row[idColumn] = obj.ID;
                    resultTable.Rows.Add(row);
                    continue;
                }
                row[nameColumn] = obj.Name;
                row[createDateColumn] =
                    ((DateTime) obj.CreateDate).ToString("dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                row[dateLastModifiedColumn] =
                    ((DateTime) obj.DateLastModified).ToString("dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                row[idColumn] = obj.ID;

                resultTable.Rows.Add(row);
            }

            List<Button> buttons = new List<Button>();

            return GetPanelFromDataTable(resultTable, parameters, buttons, Icon.Key);
        }


        private Panel ReadIndexesToPanel(string parameters, IEnumerable<object> objects, string tabName)
        {
            DataTable resultTable = new DataTable();
            resultTable.TableName = tabName;

            DataColumn nameColumn = new DataColumn("Name", typeof (string));
            DataColumn isPrimaryColumn = new DataColumn("Is Primary", typeof (String));
            DataColumn isUniqueColumn = new DataColumn("Is Unique", typeof (String));
            DataColumn isClusteredColumn = new DataColumn("Is Clustered", typeof (String));
            DataColumn isFullTextKeyColumn = new DataColumn("Is FullText Key", typeof (String));
            DataColumn idColumn = new DataColumn("ID", typeof (int));

            resultTable.Columns.Add(nameColumn);
            resultTable.Columns.Add(isPrimaryColumn);
            resultTable.Columns.Add(isUniqueColumn);
            resultTable.Columns.Add(isClusteredColumn);
            resultTable.Columns.Add(isFullTextKeyColumn);
            resultTable.Columns.Add(idColumn);

            foreach (dynamic obj in objects)
            {
                DataRow row = resultTable.NewRow();
                row[nameColumn] = obj.Name;
                if (obj.IndexKeyType.ToString() == "DriPrimaryKey")
                {
                    row[isPrimaryColumn] = true;
                }
                else
                {
                    row[isPrimaryColumn] = false;
                }

                row[isUniqueColumn] = obj.IsUnique;
                row[isClusteredColumn] = obj.IsClustered;
                row[isFullTextKeyColumn] = obj.IsFullTextKey;
                row[idColumn] = obj.ID;

                resultTable.Rows.Add(row);
            }

            List<Button> buttons = new List<Button>();

            return GetPanelFromDataTable(resultTable, parameters, buttons, Icon.TextIndent);
        }


        private Panel ReadDatabasesToPanel(string parameters, IEnumerable<object> objects, string tabName)
        {
            DataTable resultTable = new DataTable();
            resultTable.TableName = tabName;

            DataColumn nameColumn = new DataColumn("Name", typeof (string));
            DataColumn createDateColumn = new DataColumn("Created date", typeof (string));
            DataColumn dataSpaceUsageColumn = new DataColumn("Data Space Usage (Kb)", typeof (double));
            DataColumn sizeColumn = new DataColumn("Size (Mb)", typeof (double));
            DataColumn collationColumn = new DataColumn("Collation", typeof (string));
            DataColumn idColumn = new DataColumn("ID", typeof (int));

            resultTable.Columns.Add(nameColumn);
            resultTable.Columns.Add(createDateColumn);
            resultTable.Columns.Add(dataSpaceUsageColumn);
            resultTable.Columns.Add(sizeColumn);
            resultTable.Columns.Add(collationColumn);
            resultTable.Columns.Add(idColumn);

            foreach (dynamic obj in objects)
            {
                DataRow row = resultTable.NewRow();
                row[nameColumn] = obj.Name;
                row[createDateColumn] =
                    ((DateTime) obj.CreateDate).ToString("dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                row[dataSpaceUsageColumn] = obj.DataSpaceUsage;
                row[sizeColumn] = obj.Size;
                row[collationColumn] = obj.Collation;
                row[idColumn] = obj.ID;

                resultTable.Rows.Add(row);
            }

            List<Button> buttons = new List<Button>();

            return GetPanelFromDataTable(resultTable, parameters, buttons, Icon.Database);
        }


        private Panel ReadViewDataToPanel(string parameters)
        {
            int databaseId = TreeService.GetObjectIdFromParameters(parameters, "Database");
            ;
            int viewId = TreeService.GetObjectIdFromParameters(parameters, "View");

            DataTable resultTable = Connector.ReadViewData(databaseId, viewId);

            if (parameters.Contains('-'))
            {
                return GetPanelFromDataTable(resultTable, resultTable.TableName, GetButtonsForView(parameters));
            }


            return GetPanelFromDataTable(resultTable, parameters, GetButtonsForView(parameters),
                Icon.ApplicationViewList);
        }


        // ------------------------------------------------------------------------------------------------------------

        private Button GetCreateButton(string parameters, string typeName)
        {
            Button createButton = new Button();
            createButton.Icon = Icon.ScriptAdd;

            int databaseId = TreeService.GetObjectIdFromParameters(parameters, "Database");
            string currentDatabaseName = Connector.SqlConnector.GetDatabase(databaseId).Name;

            string buttonText = "Create New ";
            string createTemplate = "CREATE";
            switch (typeName)
            {
                case "Table":
                    createTemplate = "CREATE TABLE " + currentDatabaseName + @".dbo.table1 ( )";
                    buttonText = buttonText + typeName;
                    break;
                case "View":
                    createTemplate = "CREATE VIEW " + currentDatabaseName + @".dbo.view1 AS";
                    buttonText = buttonText + typeName;
                    break;
                case "StoredProcedure":
                    createTemplate = "CREATE PROCEDURE " + currentDatabaseName + @".dbo.procedure1 AS";
                    buttonText = buttonText + "Procedure";
                    break;
                case "UserDefinedFunction":
                    createTemplate = "CREATE FUNCTION " + currentDatabaseName + @".dbo.function1 ( )";
                    buttonText = buttonText + "Function";
                    break;
            }

            createButton.ToolTip = buttonText;

            RouteValueDictionary routeValues = new RouteValueDictionary();
            routeValues.Add("defaultQueryText", createTemplate);
            createButton.DirectEvents.Click.Url =
                UrlHelper.GenerateUrl(null, "AddQuery", "Manager", routeValues, RouteTable.Routes,
                    HttpContext.Current.Request.RequestContext, false);

            return createButton;
        }

        private List<Button> GetButtonsForTable(string parameters)
        {
            List<Button> buttons = new List<Button>();

            buttons.Add(GetCreateButton(parameters, "Table"));

            Button editDataInTableButton = new Button();
            editDataInTableButton.ToolTip = "Edit Table";
            editDataInTableButton.Icon = Icon.ScriptEdit;
            buttons.Add(editDataInTableButton);
            RouteValueDictionary editBuutonRouteValues = new RouteValueDictionary();
            editBuutonRouteValues.Add("parameters", parameters);
            editDataInTableButton.DirectEvents.Click.Url =
                UrlHelper.GenerateUrl(null, "GetDataFromTable", "Manager", editBuutonRouteValues, RouteTable.Routes,
                    HttpContext.Current.Request.RequestContext, false);

            return buttons;
        }

        private List<Button> GetButtonsForView(string parameters)
        {
            List<Button> buttons = new List<Button>();
            buttons.Add(GetCreateButton(parameters, "View"));
            return buttons;
        }

        private List<Button> GetButtonsForFunction(string parameters)
        {
            List<Button> buttons = new List<Button>();
            buttons.Add(GetCreateButton(parameters, "UserDefinedFunction"));
            return buttons;
        }

        private List<Button> GetButtonsForStoredProcedure(string parameters)
        {
            List<Button> buttons = new List<Button>();
            buttons.Add(GetCreateButton(parameters, "StoredProcedure"));
            return buttons;
        }


        // ------------------------------------------------------------------------------------------------------------

        private Panel GetFunctionStructureToPanel(string parameters)
        {
            int databaseId = TreeService.GetObjectIdFromParameters(parameters, "Database");
            int userDefinedFunctionId = TreeService.GetObjectIdFromParameters(parameters, "UserDefinedFunction");
            return GetPanelFromObject(parameters, Connector.SqlConnector.GetUserDefinedFunction(databaseId,
                userDefinedFunctionId), GetButtonsForFunction(parameters), Icon.Basket);
        }

        private Panel GetStoredProcedureStructureToPanel(string parameters)
        {
            int databaseId = TreeService.GetObjectIdFromParameters(parameters, "Database");
            int storedProcedureId = TreeService.GetObjectIdFromParameters(parameters, "StoredProcedure");
            return GetPanelFromObject(parameters,
                Connector.SqlConnector.GetStoredProcedure(databaseId, storedProcedureId),
                GetButtonsForStoredProcedure(parameters),
                Icon.Package);
        }

        private Panel GetPanelFromObject(string parameters, dynamic obj, IEnumerable<Button> buttons,
            Icon icon = Icon.None)
        {
            parameters = TreeService.DeleteKeyFromParameters(parameters, "Root");

            Panel newPanel = new Panel()
            {
                ID = parameters.Replace("-", ""),
                Title = obj.Name,
                Closable = true,
                BodyPadding = 6,
                Icon = icon
            };

            Toolbar toolbar = new Toolbar();
            toolbar.Items.Add(buttons);
            newPanel.Items.Add(toolbar);

            StringBuilder textateaBuilder =
                new StringBuilder("<div style=\"width:100%;height:100%;\">");

            textateaBuilder.Append("<p>Header:</p>");
            textateaBuilder.Append(
                "<textarea style=\"width:100%;height:15%; padding:5px; overflow:auto; max-width: 100%\">");
            textateaBuilder.Append(obj.TextHeader);
            textateaBuilder.Append("</textarea>");

            textateaBuilder.Append("<p>Body:</p>");
            textateaBuilder.Append(
                "<textarea style=\"width:100%;height:70%; padding:5px; overflow:auto; max-width: 100%\">");
            textateaBuilder.Append(obj.TextBody);
            textateaBuilder.Append("</textarea>");

            textateaBuilder.Append("</div>");

            newPanel.Html = textateaBuilder.ToString();

            return newPanel;
        }


        public void SaveDataTableToServer(string parameters, DataSet dataSetFromServer, ChangedDataTable changedTable)
        {
            int databaseId = TreeService.GetObjectIdFromParameters(parameters, "Database");
            int tableId = TreeService.GetObjectIdFromParameters(parameters, "Table");

            DataTable tableFromServer = dataSetFromServer.Tables[0];

            try
            {
                if (changedTable.Created != null)
                {
                    foreach (Dictionary<string, string> created in changedTable.Created)
                    {
                        DataRow newRow = tableFromServer.NewRow();
                        foreach (KeyValuePair<string, string> pair in created)
                        {
                            newRow[pair.Key] = pair.Value;
                        }
                        tableFromServer.Rows.Add(newRow);
                    }
                }
                if (changedTable.Updated != null)
                {
                    foreach (Dictionary<string, string> updated in changedTable.Updated)
                    {
                        foreach (DataRow currentRow in tableFromServer.Rows)
                        {
                            if (currentRow["ExtNetMvcId"].ToString() == updated["ExtNetMvcId"].ToString())
                            {
                                foreach (KeyValuePair<string, string> pair in updated)
                                {
                                    currentRow[pair.Key] = pair.Value;
                                }
                                break;
                            }
                        }
                    }
                }
                if (changedTable.Deleted != null)
                {
                    foreach (Dictionary<string, string> deleted in changedTable.Deleted)
                    {
                        foreach (DataRow currentRow in tableFromServer.Rows)
                        {
                            if (currentRow.RowState == DataRowState.Deleted)
                            {
                                continue;
                            }
                            if (currentRow["ExtNetMvcId"].ToString() == deleted["ExtNetMvcId"].ToString())
                            {
                                currentRow.Delete();
                                break;
                            }
                        }
                    }
                }

                tableFromServer.Columns.Remove("ExtNetMvcId");

                Connector.UpdateDataTable(databaseId, tableId, tableFromServer);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }


        private Panel GetServerInfoPanel()
        {
            Panel serverInfoPanel = new Panel();
            serverInfoPanel.ID = "ServerInfoPanel";
            serverInfoPanel.Title = "Information";
            serverInfoPanel.Padding = 5;
            serverInfoPanel.Closable = true;
            serverInfoPanel.AutoScroll = true;

            serverInfoPanel.Html = "<div style='padding:5px;padding-left:10px;'>";

            serverInfoPanel.Html = serverInfoPanel.Html + "<h2>Server Information:</h2>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>Server name: " + Connector.SqlConnector.MsSqlServer.Name +
                                   "</b><br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>Version:</b> " +
                                   Connector.SqlConnector.MsSqlServer.Version +
                                   "<br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>ProductLevel:</b> " +
                                   Connector.SqlConnector.MsSqlServer.ProductLevel +
                                   "<br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>Language:</b> " +
                                   Connector.SqlConnector.MsSqlServer.Language +
                                   "<br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>CLR version:</b> " +
                                   Connector.SqlConnector.MsSqlServer.BuildClrVersionString + "<br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>OS version:</b> " +
                                   Connector.SqlConnector.MsSqlServer.OSVersion +
                                   "<br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>Resource Version:</b> " +
                                   Connector.SqlConnector.MsSqlServer.ResourceVersion + "<br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>Edition:</b> " +
                                   Connector.SqlConnector.MsSqlServer.Edition +
                                   "<br/>";

            serverInfoPanel.Html = serverInfoPanel.Html + "<br/><b><u>Hardware:</u></b><br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>Platform:</b> " +
                                   Connector.SqlConnector.MsSqlServer.Platform +
                                   "<br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>Processor count:</b> " +
                                   Connector.SqlConnector.MsSqlServer.Processors +
                                   "<br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>Physical Memory:</b> " +
                                   Connector.SqlConnector.MsSqlServer.PhysicalMemory + " (MB)<br/>";

            serverInfoPanel.Html = serverInfoPanel.Html + "<br/><b><u>Description:</u></b><br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>Server name:</b> " +
                                   Connector.SqlConnector.MsSqlServer.NetName +
                                   "<br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>Computer NETBIOS name:</b> " +
                                   Connector.SqlConnector.MsSqlServer.ComputerNamePhysicalNetBIOS + "<br/>";

            serverInfoPanel.Html = serverInfoPanel.Html + "<br/><b><u>Configuration:</u></b><br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>Collation:</b> " +
                                   Connector.SqlConnector.MsSqlServer.Collation +
                                   "<br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>Is clustered:</b> " +
                                   Connector.SqlConnector.MsSqlServer.IsClustered +
                                   "<br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>Is HADR enabled:</b> " +
                                   Connector.SqlConnector.MsSqlServer.IsHadrEnabled +
                                   "<br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>Is full-text installed:</b> " +
                                   Connector.SqlConnector.MsSqlServer.IsFullTextInstalled + "<br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>Is single user:</b> " +
                                   Connector.SqlConnector.MsSqlServer.Information.IsSingleUser + "<br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>Login Mode:</b> " +
                                   Connector.SqlConnector.MsSqlServer.LoginMode +
                                   "<br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>SQL character set:</b> " +
                                   Connector.SqlConnector.MsSqlServer.SqlCharSetName + "<br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>SQL sort order:</b> " +
                                   Connector.SqlConnector.MsSqlServer.SqlSortOrderName + "<br/>";

            serverInfoPanel.Html = serverInfoPanel.Html + "<h2>Connection Information:</h2>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>Connection Context:</b> " +
                                   Connector.SqlConnector.MsSqlServer.ConnectionContext + "<br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>Connection timeout:</b> " +
                                   Connector.SqlConnector.MsSqlServer.ConnectionContext.ConnectTimeout + "<br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>Packet size:</b> " +
                                   Connector.SqlConnector.MsSqlServer.ConnectionContext.PacketSize + "<br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>Process ID:</b> " +
                                   Connector.SqlConnector.MsSqlServer.ConnectionContext.ProcessID + "<br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>Network protocol:</b> " +
                                   Connector.SqlConnector.MsSqlServer.ConnectionContext.NetworkProtocol + "<br/>";
            serverInfoPanel.Html = serverInfoPanel.Html + "<b>Connection encrypt:</b> " +
                                   Connector.SqlConnector.MsSqlServer.ConnectionContext.EncryptConnection + "<br/>";

            serverInfoPanel.Html = serverInfoPanel.Html + "</div>";

            return serverInfoPanel;
        }


        public Ext.Net.MVC.PartialViewResult GetDataFromTable(string parameters)
        {
            List<ModelField> fields = new ItemsCollection<ModelField>();
            List<ColumnBase> columns = new List<ColumnBase>();

            columns.Add(Html.X().RowNumbererColumn());

            int databaseId = TreeService.GetObjectIdFromParameters(parameters, "Database");
            int tableId = TreeService.GetObjectIdFromParameters(parameters, "Table");

            DataTable resultTable = Connector.ReadTableData(databaseId, tableId);

            foreach (DataColumn column in resultTable.Columns)
            {
                fields.Add(new ModelField(column.ColumnName));

                Ext.Net.Column extNetColumn = null;

                if (column.AutoIncrement)
                {
                    extNetColumn = Html.X().Column().Text(column.ColumnName)
                        .DataIndex(column.ColumnName);
                }
                else
                {
                    extNetColumn = Html.X().Column().Text(column.ColumnName)
                        .Editor(Html.X().TextField())
                        .DataIndex(column.ColumnName);
                }

                extNetColumn.Flex = 1;
                columns.Add(extNetColumn);
            }

            DataColumn extNetMvcIdColumn = new DataColumn();
            extNetMvcIdColumn.ColumnName = "ExtNetMvcId";
            extNetMvcIdColumn.DataType = typeof (Int32);

            resultTable.Columns.Add(extNetMvcIdColumn);

            for (int a = 0; a < resultTable.Rows.Count; a++)
            {
                resultTable.Rows[a]["ExtNetMvcId"] = a;
            }

            DataTableModel model = new DataTableModel();

            model.CurrentTable = resultTable;
            model.Fields = fields;
            model.Columns = columns;
            model.Parameters = parameters;
            model.Buttons = new List<Button>();

            HttpContext.Current.Session[parameters] = resultTable.DataSet;

            Ext.Net.MVC.PartialViewResult result = new Ext.Net.MVC.PartialViewResult
            {
                ViewName = "_PanelForGrid",
                ContainerId = "MainTabPanel",
                Model = model,
                RenderMode = RenderMode.AddTo
            };

            return result;
        }


        public bool DeleteData(string node, string nodeType)
        {
            return Connector.DeleteData(node, nodeType);
        }


        public Panel GetPanelFromQuery(string queryTxt, int index)
        {
            DataTable resultTable = null;
            try
            {
                DataSet resultDataSet = Connector.ExecuteQuery(queryTxt);
                resultTable = resultDataSet.Tables[0];
            }
            catch
            {
                return null;
            }

            //Строим форму вывода
            ModelField[] fields = new ModelField[resultTable.Columns.Count];
            for (int i = 0; i < resultTable.Columns.Count; i++)
            {
                fields[i] = new ModelField(resultTable.Columns[i].ColumnName);
            }

            ColumnBase[] columns = new ColumnBase[resultTable.Columns.Count];
            for (int i = 0; i < resultTable.Columns.Count; i++)
            {
                columns[i] = Html.X().Column().Text(resultTable.Columns[i].ColumnName)
                    .Editor(Html.X().TextField())
                    .DataIndex(resultTable.Columns[i].ColumnName);
            }

            Panel newPanel = new Panel()
            {
                ID = "queryResult" + index,
                Title = "Query Result " + index,
                Closable = true,
                AutoScroll = true,
                Items =
                {
                    Html.X().GridPanel()
                        .Title("Result" + index)
                        .Store(Html.X().Store()
                            .ID("Store1")
                            .Model(Html.X().Model()
                                .IDProperty("ID")
                                .Fields(fields)
                            )
                            .DataSource(resultTable)
                        )
                        .ColumnModel(columns)
                        .SelectionModel(Html.X().CellSelectionModel())
                }
            };

            return newPanel;
        }


        public Panel GetPanelToQuery(string defaultQueryText, int index)
        {
            RouteValueDictionary routeValues = new RouteValueDictionary();

            string runQueryUrl =
                UrlHelper.GenerateUrl(null, "RunQuery", "Manager", routeValues, RouteTable.Routes,
                    HttpContext.Current.Request.RequestContext, false);

            Panel newPanel = new Panel()
            {
                ID = "query" + index,
                Title = "Query " + index,
                Closable = true,
                AutoScroll = true,
                Items =
                {
                    Html.X().FormPanel()
                        .DefaultAnchor("100%")
                        .Frame(true)
                        .Items(
                            Html.X().TextArea()
                                .Editable(true)
                                .ID("QueryTxt" + index)
                                .Width(500)
                                .Height(400)
                                .Text(defaultQueryText)
                        )
                        .Buttons(Html.X().Button()
                            .ID("Run" + index)
                            .Text("Run")
                            .Icon(Icon.Accept)
                            .DirectEvents(
                                de =>
                                {
                                    de.Click.Url = runQueryUrl;
                                    de.Click.ExtraParams.Add(new {currentIndex = index});
                                })
                            .Listeners(ls => ls.Click.Fn = "reloadDatabaseTree")
                        )
                }
            };

            return newPanel;
        }
    }
}