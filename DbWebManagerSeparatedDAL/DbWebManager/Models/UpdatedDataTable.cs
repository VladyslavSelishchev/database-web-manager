﻿using System.Collections.Generic;

namespace DbWebManager.Models
{
    public class ChangedDataTable
    {
        public List<Dictionary<string, string>> Updated { get; set; }

        public List<Dictionary<string, string>> Deleted { get; set; }

        public List<Dictionary<string, string>> Created { get; set; }
    }
}