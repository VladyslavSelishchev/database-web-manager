﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DbWebManager.BLL;
using DbWebManager.Models;
using Ext.Net;
using Ext.Net.MVC;
using Microsoft.SqlServer.Management.Smo;
using DbWebManager.DAL;

namespace DbWebManager.Controllers
{
    public class LoginController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Try connect to MS SQL Server
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Connect(MsSqlConnectionModel model)
        {
            if (ModelState.IsValid && model != null)
            {
                try
                {
                    IMsSqlConnector manager = new MsSqlConnector(model);
                    int buildNumber = manager.MsSqlServer.ConnectionContext.ServerVersion.BuildNumber;
                    HttpContext.Session["ConnectionModel"] = model;
                }
                catch (Exception Ex)
                {
                    HttpContext.Cache["Errors"] = Ex.Message;
                    X.Msg.Alert("Error", HttpContext.Cache["Errors"]).Show();
                    return this.Direct();
                }

                return RedirectToAction("Index", "Manager");
            }


            X.Msg.Alert("Error!", "Please check your input data!").Show();
            return this.Direct();
        }
    }
}